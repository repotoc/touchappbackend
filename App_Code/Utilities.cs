﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Utilities
/// </summary>
public class Utilities
{
	public Utilities()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static void imageToByteArray(System.Drawing.Image imageIn, string item,string destination,string copiedFolder)
    {
        string extension = System.IO.Path.GetExtension(item);

        if (extension.ToUpper() == ".PNG")
        {
            imageIn = imageIn.GetThumbnailImage(160, 220, null, IntPtr.Zero);
            if (!string.IsNullOrEmpty(destination))
                imageIn.Save(destination, System.Drawing.Imaging.ImageFormat.Png);
            if (!string.IsNullOrEmpty(copiedFolder))
                imageIn.Save(copiedFolder, System.Drawing.Imaging.ImageFormat.Png);
        }
    }
}