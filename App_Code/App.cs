﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyfashionsDB.DBFolder;
using System.Web;
using NLog;

/// <summary>
/// Summary description for App
/// </summary>
public class App
{
    public static MyfashionsDBContext  myFashionsDBContext=new MyfashionsDBContext();
    public static Logger Logger = LogManager.GetLogger("TouchBackEnd");
}