﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>touchOnCloud</title>
    
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="css/alertify.min.css" rel="stylesheet" />
    <link href="css/themes/default.rtl.css" rel="stylesheet" />    
    <script src="Js/alertify.min.js"></script>
    <link href="css/font-awesome.min.css" rel="stylesheet"/>
    <script type="text/javascript">
        function successAlert(msg) {
            alertify.success(msg);
        }
        function warningAlert(msg) {
            alertify.warning(msg);
        }
        function errorAlert(msg) {
            alertify.error(msg);
        }
        function messageAlert(msg) {
            alertify.message(msg);
        }
    </script>
</head>
<body>
   <form id="form1" runat="server" role="form" autocomplete="off">
        <div class="col-xs-8 col-sm-8 col-md-3 jumbotron col-centered" style="margin-top: 12%;">
            <div class="form-group">
                <asp:Button ID="btnSuperAdmin" runat="server" Text="SuperAdmin"  class="btn btn-success btn-md btn-block" OnClick="btnSuperAdmin_Click"/>
                <asp:Button ID="btnCompanyAdmin" runat="server" Text="Company Admin" class="btn btn-default btn-md btn-block" OnClick="btnCompanyAdmin_Click"/>
                <asp:Button ID="btnStoreAdmin" runat="server" Text="Store Admin" class="btn btn-default btn-md btn-block" OnClick="btnStoreAdmin_Click"/>
            </div>
        </div>
    </form>
</body>
</html>
