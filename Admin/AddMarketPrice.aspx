﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="AddMarketPrice.aspx.cs" Inherits="Admin_AddMarketPrice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        $(document).ready(function () {

            //CHECKING OF LOCALSTORAGE AND PREPENDING OF SELECT BOXES
            var dc = localStorage.getItem("noof");
            if (localStorage.getItem("htmldata0") != " " && localStorage.getItem("htmldata0") !== "undefined") {
                for (i = 0; i <= parseInt(dc) ; i++) {
                    var htmldata = localStorage.getItem("htmldata" + i + "");

                    $(".dsata").append("<select class='slctdvalue1 form-control mg-top' name=" + i + ">" + htmldata + "</select>");
                }
                //SETTING OF VALUES FOR DROPDOWN BY USING LOACLSTORAGE
                for (i = 0; i <= parseInt(dc) ; i++) {
                    //alert(localStorage.getItem("dfltdata1"));

                    var a = localStorage.getItem("dfltdata" + i + "");
                    $(".slctdvalue1 option").each(function (e, i) {
                        var b = $(this).val();

                        if (a == b) {
                            $(this).parent().val(a);
                        }
                    });
                }
            }

            //FIRST TEXTBOX CHANGE FUNCTION
            $(".slctdvalue").change(function () {
                $(".hdnvalue").val($(this).val());
                $(".hdnvalue").change();
            })

            //REST OF TEXTBOX CHANGE FUNCTION AND CHANGE FUNCTION
            $(document).on("change", ".slctdvalue1", function () {

                $(".hdnvalue").val($(this).val());
                $(".hdnvalue").change();
                $(".slctdvalue1").each(function (e, i) {

                    localStorage.setItem("htmldata" + e + "", $(this).html());
                    localStorage.setItem("dfltdata" + e + "", $(this).val());

                    localStorage.setItem("noof", e);
                });
            })

        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ol class="breadcrumb">
        <li><a href="AdminHome.aspx">Home</a></li>
        <li>Filters</li>
        <li class="active">Add Filters</li>
    </ol>
     <div class="row form-group">
        <div class="col-sm-12 col-md-5 form-group">
            <ul id="noRecord" runat="server" visible="false" class="list-group" style="text-align: center;">
                <li class="list-group-item">No Records Found</li>
            </ul>
            <asp:PlaceHolder ID="PlaceHolder1" runat="server" />
            <div class="dsata"></div>
            <asp:PlaceHolder ID="PlaceHolder2" runat="server" />
            <asp:TextBox ID="hdntxtvalue" runat="server" OnTextChanged="hdntxtvalue_TextChanged" AutoPostBack="true" CssClass="hdnvalue" Style="display: none;" />
            <asp:TextBox ID="hdnvaluedync" runat="server" CssClass="hdnvalue1" Style="display: none;" />
        </div>
        <div class="col-sm-12 col-md-7 form-group">
            <div class="col-sm-4 col-md-4 form-group">Price</div>
            <div class="col-sm-8 col-md-8 form-group"><asp:TextBox ID="txtPrice" runat="server" CssClass="form-control" /></div>
            <div class="col-sm-4 col-md-4 form-group">Notation</div>
            <div class="col-sm-8 col-md-8 form-group"><asp:TextBox ID="txtNOtation" runat="server" CssClass="form-control"></asp:TextBox></div>
              <div class="col-sm-4 col-md-4 form-group">Notes</div>
            <div class="col-sm-8 col-md-8 form-group"><asp:TextBox ID="txtNotes" runat="server" CssClass="form-control" /></div>
            <div class="col-sm-4 col-md-4 form-group"></div>
            <div class="col-sm-8 col-md-8 form-group"><asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn btn-success" /></div>
        </div>
    </div>
</asp:Content>

