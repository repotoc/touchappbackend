﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ViewStores.aspx.cs" Inherits="Admin_ViewStores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ol class="breadcrumb">
        <li><a href="AdminHome.aspx">Home</a></li>
        <li>Stores</li>
        <li class="active">View Stores</li>
    </ol>

    <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="table-responsive">
            <asp:GridView ID="gvStores" runat="server" AutoGenerateColumns="false" DataKeyNames="ID"
                CssClass="table table-striped table-bordered" AllowPaging="true" PageSize="10"
                OnPageIndexChanging="gvStores_PageIndexChanging"
                OnRowCommand="gvStores_RowCommand"
                OnRowEditing="gvStores_RowEditing"
                OnRowDeleting="gvStores_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("ID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Image ID="Image" runat="server" ImageUrl='<%# String.Format("~/SuperAdmin/Logo/{0}", Eval("IMAGENAME")) %>'
                                    Height="64px" Width="100px" class="img-responsive" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="USERNAME">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("USERNAME") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NAME">
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%#Eval("NAME") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ISACTIVE">
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%#Eval("ISACTIVE") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NUMEMPLOYEES">
                        <ItemTemplate>
                            <asp:Label ID="Label5" runat="server" Text='<%#Eval("NUMEMPLOYEES") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="VERSION">
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text='<%#Eval("VERSION") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="LASTUPDATEDTIME">
                        <ItemTemplate>
                            <asp:Label ID="Label7" runat="server" Text='<%#Eval("LASTUPDATEDTIME") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ADDRESS">
                        <ItemTemplate>
                            <div>
                                <p>
                                    <asp:Label ID="Label8" runat="server" Text='<%# string.Format("{0},", Eval("ADDRES1")) %>' />
                                    <asp:Label ID="Label9" runat="server" Text='<%# string.Format("{0},", Eval("CITY")) %>' />
                                    <asp:Label ID="Label10" runat="server" Text='<%#Eval("STATE") %>' /></p>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <div class="col-wd-70">
                                <div class="ed-del-col">
                                    <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Icons/edit.png" CommandName="EDIT" CommandArgument='<%#Eval("ID") %>'
                                        Style="height: 18px;" data-toggle="tooltip" data-placement="bottom" title="EDIT" />
                                </div>
                                <div  class="ed-del-col">
                                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/Icons/delete.png" CommandName="DELETE" CommandArgument='<%#Eval("ID") %>'
                                        OnClientClick="return ConfirmDelete();" Style="height: 18px;" data-toggle="tooltip" data-placement="bottom" title="DELETE" />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                   <%-- <asp:TemplateField HeaderText="DELETE">
                        <ItemTemplate>
                           </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
                <PagerStyle CssClass="Pagination" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" />
                <EmptyDataTemplate>
                    <ul class="list-group" style="text-align: center;">
                        <li class="list-group-item">No Records Found</li>
                    </ul>
                </EmptyDataTemplate>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

