﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ViewCateg : System.Web.UI.Page
{
    DropDownList ddlMain;
    object lockTarget = new object();
    string sGuid;
    string frameURI;
    HttpClient client;
    public static List<MasterCategories> master;
    public static MainEnum main;
    string errorMsg;
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;

        if (!IsPostBack)
        {
            sGuid = Guid.NewGuid().ToString();
            hdGuid.Value = sGuid;
            LoadUpdate();
        }
        ddlMain = new DropDownList();
        ddlMain.ID = "ddlMain";
        ddlMain.AutoPostBack = true;
        ddlMain.DataTextField = "NAME";
        ddlMain.DataValueField = "SNO";
        ddlMain.CssClass = "slctdvalue form-control";
        try
        {
            string cIDq = Session["companyadminid"].ToString();
            long cID = Convert.ToInt64(Session["companyadminid"]);
            string uri = "api/MasterCategories/GetAllCategories?relatedcompanyid=" + cID;// +"&fetchTopCategories=true";
            HttpResponseMessage response = client.GetAsync(uri).Result;
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                main = response.Content.ReadAsAsync<MainEnum>().Result;
                ddlMain.DataSource = main.LSTCATEGORIES;
                ddlMain.DataBind();
                ddlMain.Items.Insert(0, "Select");
                //ddlMain.SelectedIndexChanged += new EventHandler(Dynamic_Method);
                PlaceHolder1.Controls.Add(ddlMain);
            }
            else
            {
                string errorMsg = response.Content.ReadAsStringAsync().Result;
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void hdntxtvalue_TextChanged(object sender, EventArgs e)
    {
        TextBox selectedText = ((TextBox)sender); 
        divEditPanel.Visible = true;
        if (selectedText != null)
        {
            if (selectedText.Text != "Select")
            {
                long selectCategId = Convert.ToInt64(selectedText.Text);

                string uri = "api/MasterCategories/GetSubCategories?relatedmastercategoryid=" + selectCategId + "";
                HttpResponseMessage response = client.GetAsync(uri).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    LoadUpdate();
                    master = response.Content.ReadAsAsync<List<MasterCategories>>().Result;                  
                    int cnt = FindOccurence("ddl");
                    long selectedddlcnt = cnt + 1;
                    if (master.Count > 0)
                        BuildDynamicDropDown(master, selectedddlcnt);

                    long ID = Convert.ToInt64(hdntxtvalue.Text);
                    string uriSingleCateg = "api/MasterCategories/GetSingleCategory?relatedcategoryid=" + ID + "";
                    response = client.GetAsync(uriSingleCateg).Result;
                    if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                    {
                        MasterCategories mCategory = response.Content.ReadAsAsync<MasterCategories>().Result;
                        string json = Newtonsoft.Json.JsonConvert.SerializeObject(mCategory);
                        //if (mCategory.SELECTEDCATEG == null)
                        //    btnEditUpdate.Visible = false;
                        //else
                        //    btnEditUpdate.Visible = true;
                        if (mCategory != null)
                        {
                            txtCategoryName.Text = mCategory.NAME;
                            imgCtg.ImageUrl = @"~/MainCategories/" + mCategory.IMAGENAME;
                        }
                    }
                    else
                    {
                        errorMsg = response.Content.ReadAsStringAsync().Result;
                    }
                }
                else
                {
                    errorMsg = response.Content.ReadAsStringAsync().Result;
                }
            }
        }
    }
    private int FindOccurence(string substr)
    {
        string reqstr = Request.Form.ToString();
        return ((reqstr.Length - reqstr.Replace(substr, "").Length) / substr.Length);
    }
    public static int i = 0;
    private void BuildDynamicDropDown(IList<MasterCategories> list, long uniqueID)
    {
        DropDownList ddl = new DropDownList();
        ddl.ID = "ddl" + uniqueID;
        ddl.DataSource = list;
        ddl.DataTextField = "NAME";
        ddl.DataValueField = "SNO";
        ddl.CssClass = "slctdvalue1 form-control mg-top";
        ddl.EnableViewState = true;
        ddl.DataBind();
        ddl.Items.Insert(0, new ListItem("Select", "Select", true));
        PlaceHolder2.Controls.Add(ddl);
    }

    protected void btnEditUpdate_Click(object sender, EventArgs e)
    {
        if (btnEditUpdate.Text == "Edit")
        {
            string ID = hdntxtvalue.Text;
            if (!string.IsNullOrEmpty(ID))
                LoadEdit();
        }
        else if (btnEditUpdate.Text == "Update")
        {
            ddlMain.ClearSelection();
            long ID = Convert.ToInt64(hdntxtvalue.Text);
            string uriSingleCateg = "api/MasterCategories/GetSingleCategory?relatedcategoryid=" + ID + "";
            HttpResponseMessage response = client.GetAsync(uriSingleCateg).Result;
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                MasterCategories mCategory = response.Content.ReadAsAsync<MasterCategories>().Result;
                if (mCategory != null)
                {
                    mCategory.NAME = txtCategoryName.Text;
                    if (fuImage.HasFile)
                    {
                        string fileName = Path.GetFileName(fuImage.PostedFile.FileName);
                        string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtCategoryName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                        string path = @"D:\Client Management Component\Users\TouchBackEndImages1";
                        string folder = path + @"\MainCategories";
                        string ImagesFolder = "MainCategories";
                        string ServerPath = Server.MapPath("~/MainCategories");
                        if (!Directory.Exists(ServerPath))
                        {
                            Directory.CreateDirectory(ServerPath);
                        }
                        fuImage.PostedFile.SaveAs(Path.Combine(ServerPath, hdGuid.Value + "~" + strRandomNumber));

                        if (!Directory.Exists(folder))
                        {
                            Directory.CreateDirectory(folder);
                        }
                        fuImage.PostedFile.SaveAs(Path.Combine(folder, hdGuid.Value + "~" + strRandomNumber));
                        mCategory.IMAGENAME = hdGuid.Value + "~" + strRandomNumber;
                        mCategory.IMAGELOCATION = "pack://siteoforigin:,,,/" + ImagesFolder + "/" + hdGuid.Value + "~" + strRandomNumber;
                        mCategory.WEBIMAGELOCATION = folder + hdGuid.Value + "~" + strRandomNumber;
                    }
                    if (chkActive.Checked)
                        mCategory.ISACTIVE = true;
                    else
                        mCategory.ISACTIVE = false;
                    mCategory.SELECTEDENUM = mCategory.SELECTEDENUM;
                    mCategory.SELECTEDCATEG = mCategory.SELECTEDCATEG;
                    mCategory.VERSION = mCategory.VERSION + 1;
                    mCategory.LASTUPDATEDTIME = DateTime.Now;

                    string uri = "api/MasterCategories/MasterCategoriesCRUDOperation?parameter=U";
                    HttpResponseMessage responseU = client.PostAsJsonAsync<MasterCategories>(uri, mCategory).Result;
                    if (responseU.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Updated Successfully');", true);
                    }
                    else
                    {
                        string json = Newtonsoft.Json.JsonConvert.SerializeObject(mCategory);
                        errorMsg = responseU.Content.ReadAsStringAsync().Result;
                    }
                }
            }
            else
            {
                errorMsg = response.Content.ReadAsStringAsync().Result;
            }
        }
    }
    private void LoadEdit()
    {
        divFUpload.Visible = true;
        divImage.Visible = false;
        txtCategoryName.Attributes.Remove("readonly");
        btnEditUpdate.Text = "Update";
    }
    private void LoadUpdate()
    {
        divFUpload.Visible = false;
        divImage.Visible = true;
        txtCategoryName.Attributes.Add("readonly", "true");
        btnEditUpdate.Text = "Edit";
    }
}