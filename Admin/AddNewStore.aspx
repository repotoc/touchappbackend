﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="AddNewStore.aspx.cs" Inherits="Admin_AddNewStore" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ol class="breadcrumb">
        <li><a href="AdminHome.aspx">Home</a></li>
        <li>Stores</li>
        <li class="active">Add New Store</li>
    </ol>

    <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="row form-group col-md-12 col-sm-12">
            <div class="row form-group col-md-6 col-sm-12">
                    <div class="col-md-4 col-sm-12">
                        <div class="lblSideHeading">Store Id</div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <asp:TextBox ID="txtstoreId" runat="server" CssClass="form-control" placeholder="Store ID"></asp:TextBox>
                    </div>
                </div>
                <div class="row form-group col-md-6 col-sm-12">
                    <div class="col-md-4 col-sm-12">
                        <div class="lblSideHeading">Store Name</div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <asp:TextBox ID="txcompanyName" runat="server" CssClass="form-control" placeholder="Store Name"></asp:TextBox>
                    </div>
                </div>
                <div class="row form-group col-md-6 col-sm-12">
                    <div class="col-md-4 col-sm-12">
                        <div class="lblSideHeading">User Name</div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <asp:TextBox ID="txtusername" runat="server" CssClass="form-control" placeholder="User Name"></asp:TextBox>
                    </div>
                </div>
                
                <div class="row form-group col-md-6 col-sm-12">
                    <div class="col-md-4 col-sm-12">
                        <div class="lblSideHeading">Password</div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row form-group col-md-6 col-sm-12">
                    <div class="col-md-4 col-sm-12">
                        <div class="lblSideHeading">City</div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" placeholder="City"></asp:TextBox>
                    </div>
                </div>
                <div class="row form-group col-md-6 col-sm-12">
                    <div class="col-md-4 col-sm-12">
                        <div class="lblSideHeading">State</div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <asp:TextBox ID="txtSate" runat="server" CssClass="form-control" placeholder="State"></asp:TextBox>
                    </div>
                </div>
                <div class="row form-group col-md-6 col-sm-12">
                    <div class="col-md-4 col-sm-12">
                        <div class="lblSideHeading">Address Line1</div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <asp:TextBox ID="txtadd1" runat="server" CssClass="form-control" placeholder="Address Line1" />
                    </div>
                </div>
                <div class="row form-group col-md-6 col-sm-12">
                    <div class="col-md-4 col-sm-12">
                        <div class="lblSideHeading">Address Line2</div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <asp:TextBox ID="txtadd2" runat="server" class="form-control" placeholder="Address Line2" />
                    </div>
                </div>
                <div class="row form-group col-md-6 col-sm-12">
                    <div class="col-md-4 col-sm-12">
                        <div class="lblSideHeading">Logo</div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <asp:FileUpload ID="fplogo" runat="server" CssClass="imageupload form-control" data-toggle="tooltip" data-placement="bottom"  title="select only '.png' format" />
                        <asp:HiddenField ID="hdGuid" runat="server" />
                    </div>
                </div>
                <div class="row form-group col-md-6 col-sm-12">
                    <div class="col-md-4 col-sm-12">
                        <div class="lblSideHeading">No. Of Employees</div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <asp:TextBox ID="txtnoemp" runat="server" class="form-control" />
                    </div>
                </div>
                <div class="row form-group col-md-6 col-sm-12">
                    <div class="col-md-4 col-sm-12">
                        <div class="lblSideHeading"></div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <asp:CheckBox ID="chkActive" runat="server" Checked="true" Text="IsActive" />
                    </div>
                </div>
        </div>
        <div class="row form-group col-md-6 col-sm-6">
            <div class="col-md-4 col-sm-4"></div>
            <div class="col-md-8 col-sm-8">
                <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-success" OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-default" OnClick="btnCancel_Click" />
            </div>
        </div>
    </div>
</asp:Content>

