﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyfashionsDB.Models;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Data;

public partial class Admin_ViewStores : System.Web.UI.Page
{
    DataTable dtStores;
    string frameURI;
    HttpClient client;
    public static List<Stores> storesLst;
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;
        if (!IsPostBack)
        {
            LoadData();
        }
    }

    private void LoadData()
    {
        long cID = Convert.ToInt64(Session["companyadminid"]);
        string uri = "api/StoreLogin/GetAllStores?relatedcompanyid=" + cID + "";
        HttpResponseMessage response = client.GetAsync(uri).Result;
        if (response.StatusCode.Equals(HttpStatusCode.OK))
        {
            storesLst = response.Content.ReadAsAsync<List<Stores>>().Result;
            dtStores = new DataTable();
            var Columns = new[] {
                new DataColumn("ID", typeof(long)),
                new DataColumn("IMAGENAME", typeof(string)),
                new DataColumn("USERNAME", typeof(string)),
                new DataColumn("NAME", typeof(string)),
                new DataColumn("ISACTIVE", typeof(bool)),
                new DataColumn("NUMEMPLOYEES", typeof(int)),
                new DataColumn("VERSION", typeof(string)),
                new DataColumn("LASTUPDATEDTIME", typeof(DateTime)),
                new DataColumn("ADDRES1", typeof(string)),
                new DataColumn("ADDRES2", typeof(string)),
                new DataColumn("CITY", typeof(string)),
                new DataColumn("STATE", typeof(string)),
            };
            dtStores.Columns.AddRange(Columns);
            foreach (var item in storesLst.ToList().Where(c=>c.ISACTIVE).OrderByDescending(c => c.LASTUPDATEDTIME))
            {
                DataRow drStores = dtStores.NewRow();
                drStores["ID"] = item.ID;
                drStores["IMAGENAME"] = item.IMAGENAME;
                drStores["USERNAME"] = item.USERNAME;
                drStores["NAME"] = item.NAME;
                drStores["ISACTIVE"] = item.ISACTIVE;
                drStores["NUMEMPLOYEES"] = item.NUMEMPLOYEES;
                drStores["VERSION"] = item.VERSION;
                drStores["LASTUPDATEDTIME"] = item.LASTUPDATEDTIME;
                drStores["ADDRES1"] = item.ADDRES1;
                drStores["ADDRES2"] = item.ADDRESS2;
                drStores["CITY"] = item.CITY;
                drStores["STATE"] = item.STATE;
                dtStores.Rows.Add(drStores);
            }
            gvStores.DataSource = dtStores;
            gvStores.DataBind();
        }
        else
        {
            string errorMsg = response.Content.ReadAsStringAsync().Result;
        }
    }
    protected void gvStores_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvStores.PageIndex = e.NewPageIndex;
        LoadData();
    }
    protected void gvStores_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        long ID = Convert.ToInt64(e.CommandArgument);
        Stores store = storesLst.ToList().Where(c => c.ID.Equals(ID)).FirstOrDefault();
        if (store != null)
        {
            if (e.CommandName == "EDIT")
            {
                Response.Redirect("AddNewStore.aspx?ID=" + ID + "");
            }
            else if (e.CommandName == "DELETE")
            {
                #region CrudOperation
                string uriD = "api/StoreLogin/StoreCRUDOperation?parameter=D";
                HttpResponseMessage responsec = client.PostAsJsonAsync<Stores>(uriD, store).Result;
                if (responsec.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Store Deleted Successfully');", true);
                    LoadData();
                }
                else
                {
                    string errorMsg = responsec.Content.ReadAsStringAsync().Result;
                }
                #endregion
            }
        }
    }
    protected void gvStores_RowEditing(object sender, GridViewEditEventArgs e)
    {

    }
    protected void gvStores_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
}