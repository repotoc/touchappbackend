﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyfashionsDB.Models;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;

public partial class Admin_AddMainCategory : System.Web.UI.Page
{
    object lockTarget = new object();
    string sGuid;
    string frameURI;
    HttpClient client;
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;
        if (!IsPostBack)
        {
            App.Logger.Info("Starts PageLoad in AddStore Page");
            sGuid = Guid.NewGuid().ToString();
            hdGuid.Value = sGuid;
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            MasterCategories mainCategory = new MasterCategories();
            mainCategory.NAME = txtMainCategoryName.Text.Trim().ToUpper();
            if (fuMainCategoryImg.HasFile)
            {
                string fileName = Path.GetFileName(fuMainCategoryImg.PostedFile.FileName);
                string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtMainCategoryName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                string path = @"D:\Client Management Component\Users\TouchBackEndImages1";
                string folder = path + @"\MainCategories";
                string ImagesFolder = "MainCategories";

                string ServerPath = Server.MapPath("~/MainCategories");
                if (!Directory.Exists(ServerPath))
                {
                    Directory.CreateDirectory(ServerPath);
                }
                fuMainCategoryImg.PostedFile.SaveAs(Path.Combine(ServerPath, hdGuid.Value + "~" + strRandomNumber));

                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                fuMainCategoryImg.PostedFile.SaveAs(Path.Combine(folder, hdGuid.Value + "~" + strRandomNumber));
                mainCategory.IMAGENAME = hdGuid.Value + "~" + strRandomNumber;
                mainCategory.IMAGELOCATION = "pack://siteoforigin:,,,/" + ImagesFolder + "/" + hdGuid.Value + "~" + strRandomNumber;
                mainCategory.WEBIMAGELOCATION = folder + hdGuid.Value + "~" + strRandomNumber;
            }
            if (chkActive.Checked)
                mainCategory.ISACTIVE = true;
            else
                mainCategory.ISACTIVE = false;
            mainCategory.VERSION = 1;
            mainCategory.LASTUPDATEDTIME = DateTime.Now;
            
            string companyadminid = Session["companyadminid"].ToString();
            string uriCompany = "api/CompanyLogin/GetCompanyInfo?companyID=" + companyadminid + "";
            HttpResponseMessage responseCmp = client.GetAsync(uriCompany).Result;
            if (responseCmp.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                Company selectedCompany = responseCmp.Content.ReadAsAsync<Company>().Result;
                mainCategory.REGCOMPANY = selectedCompany;
            }
            else
            {
                string errorMsg = responseCmp.Content.ReadAsStringAsync().Result;
            }
            string uri = "api/MasterCategories/GetAllCategories?relatedcompanyid=" + companyadminid + "&fetchTopCategories=true";
            HttpResponseMessage responseCateg = client.GetAsync(uri).Result;
            if (responseCateg.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                MainEnum mainEnum = responseCateg.Content.ReadAsAsync<MainEnum>().Result;
                mainCategory.SELECTEDENUM = mainEnum;
            }
             string uriCreateCmp = "api/MasterCategories/MasterCategoriesCRUDOperation?parameter=C";
            HttpResponseMessage responseCreateCmp = client.PostAsJsonAsync<MasterCategories>(uriCreateCmp, mainCategory).Result;
            if (responseCreateCmp.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Main Category Created Successfully');", true);
            }
            else
            {
                string errorMsg = responseCreateCmp.Content.ReadAsStringAsync().Result;
            }
            mainCategory.SELECTEDCATEG = new MasterCategories();
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtMainCategoryName.Text = string.Empty;
        txtexist.Text = string.Empty;
    }
}