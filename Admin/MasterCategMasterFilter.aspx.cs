﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_MasterCategMasterFilter : System.Web.UI.Page
{
    DropDownList ddlMain, ddlFilters;
    object lockTarget = new object();
    string frameURI;
    HttpClient client;
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;

        #region ddlFiltersBinding
        ddlFilters = new DropDownList();
        ddlFilters.ID = "ddlFilters";
        ddlFilters.AutoPostBack = true;
        ddlFilters.DataTextField = "NAME";
        ddlFilters.DataValueField = "ID";
        ddlFilters.CssClass = "slctdvalue3 form-control ";
        try
        {

            MainEnum main = App.myFashionsDBContext.MainEnum.Where(c => c.NAME.Equals("Filters")).FirstOrDefault();
            ddlFilters.DataSource = main.LSTFILTERS;
            ddlFilters.DataBind();
            ddlFilters.Items.Insert(0, "Select");
            PlaceHolder3.Controls.Add(ddlFilters);
            //string str = Session["companyadminid"].ToString();
            //long strusername = Convert.ToInt64(Session["companyadminid"].ToString());
            //string uri = "api/MasterCategories/GetAllCategories?relatedcompanyid=" + strusername + "&fetchTopCategories=true";
            //HttpResponseMessage response = client.GetAsync(uri).Result;
            //if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            //{
            //    MainEnum main = response.Content.ReadAsAsync<MainEnum>().Result;
            //    ddlMain.DataSource = main.LSTCATEGORIES;
            //    ddlMain.DataBind();
            //    ddlMain.Items.Insert(0, "Select");
            //    PlaceHolder1.Controls.Add(ddlMain);
            //}
            //else
            //{
            //    string errorMsg = response.Content.ReadAsStringAsync().Result;
            //}
        }
        catch (Exception ex)
        {
        }
        #endregion

        #region ddlMainBinding
        ddlMain = new DropDownList();
        ddlMain.ID = "ddlMain";
        ddlMain.AutoPostBack = true;
        ddlMain.DataTextField = "NAME";
        ddlMain.DataValueField = "SNO";
        ddlMain.CssClass = "slctdvalue form-control";
        try
        {
            string str = Session["companyadminid"].ToString();
            long strusername = Convert.ToInt64(Session["companyadminid"].ToString());
            string uri = "api/MasterCategories/GetAllCategories?relatedcompanyid=" + strusername + "&fetchTopCategories=true";
            HttpResponseMessage response = client.GetAsync(uri).Result;
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                MainEnum main = response.Content.ReadAsAsync<MainEnum>().Result;
                ddlMain.DataSource = main.LSTCATEGORIES;
                ddlMain.DataBind();
                ddlMain.Items.Insert(0, "Select");
                //ddlMain.SelectedIndexChanged += new EventHandler(Dynamic_Method);
                PlaceHolder1.Controls.Add(ddlMain);
            }
            else
            {
                string errorMsg = response.Content.ReadAsStringAsync().Result;
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(GetType(), "notifierErr", "errorAlert('"+ex.Message+"');", true);
        }
        #endregion
    }

    private void Dynamic_Method(object sender, EventArgs e)
    {
        if (ddlMain.SelectedItem.Text == "Select")
        {
            rblCateg.Items.Clear();
            rblCateg.DataSource = null;
            rblCateg.DataBind();            
        }
    }
    protected void hdntxtvalue_TextChanged(object sender, EventArgs e)
    {
        TextBox selectedText = ((TextBox)sender);
        if (selectedText != null)
        {
            if (selectedText.Text != "Select")
            {
                long selectCategId = Convert.ToInt64(selectedText.Text);
                string uri = "api/MasterCategories/GetSubCategories?relatedmastercategoryid=" + selectCategId + "";
                HttpResponseMessage response = client.GetAsync(uri).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    List<MasterCategories> master = response.Content.ReadAsAsync<List<MasterCategories>>().Result;
                    int cnt = FindOccurence("ddl");
                    long selectedddlcnt = cnt + 1;
                    if (master.Count > 0)
                        BuildDynamicDropDown(master, selectedddlcnt);
                }
                else
                {
                    string errorMsg = response.Content.ReadAsStringAsync().Result;
                }
            }
        }
    }
    private int FindOccurence(string substr)
    {
        string reqstr = Request.Form.ToString();
        return ((reqstr.Length - reqstr.Replace(substr, "").Length) / substr.Length);
    }
    public static int i = 0;
    private void BuildDynamicDropDown(IList<MasterCategories> list, long uniqueID)
    {
        DropDownList ddl = new DropDownList();
        ddl.ID = "ddl" + uniqueID;
        ddl.DataSource = list;
        ddl.DataTextField = "NAME";
        ddl.DataValueField = "ID";
        ddl.CssClass = "slctdvalue1 form-control mg-top";
        ddl.EnableViewState = true;
        ddl.DataBind();
        ddl.Items.Insert(0, new ListItem("Select", "Select", true));
        PlaceHolder2.Controls.Add(ddl);
    }

    protected void hdntxtvalue2_TextChanged(object sender, EventArgs e)
    {
        TextBox selectedText = ((TextBox)sender);
        if (selectedText != null)
        {
            if (selectedText.Text != "Select")
            {
                long selectCategId = Convert.ToInt64(selectedText.Text);
                string uriSingleFilter = "api/MasterFilters/GetFilterByItsFilterID?filterID=" + selectCategId + "";
                HttpResponseMessage response = client.GetAsync(uriSingleFilter).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    MasterFilters selectedFilter = response.Content.ReadAsAsync<MasterFilters>().Result;
                    if (selectedFilter != null)
                    {
                        if (selectedFilter.LSTSUBFILTERS != null && selectedFilter.LSTSUBFILTERS.Count > 0)
                        {
                            int cnt = FindOccurenceFilter("ddlFilter");
                            long selectedddlcnt = cnt + 1;
                            BuildDynamicDropDownFilter(selectedFilter.LSTSUBFILTERS, selectedddlcnt);
                        }
                    }
                }
            }
        }
    }
    private void BuildDynamicDropDownFilter(IList<MasterFilters> list, long uniqueID)
    {
        DropDownList ddl = new DropDownList();
        ddl.ID = "ddlFilter" + uniqueID;
        ddl.DataSource = list;
        ddl.DataTextField = "NAME";
        ddl.DataValueField = "ID";
        ddl.CssClass = "slctdvalue4 form-control mg-top";
        ddl.EnableViewState = true;
        ddl.DataBind();
        ddl.Items.Insert(0, new ListItem("Select", "Select", true));
        PlaceHolder4.Controls.Add(ddl);
    }

    private int FindOccurenceFilter(string substr)
    {
        string reqstr = Request.Form.ToString();
        return ((reqstr.Length - reqstr.Replace(substr, "").Length) / substr.Length);
    }
    protected void btnSelectedCateg_Click(object sender, EventArgs e)
    {
        long SNO = Convert.ToInt64(hdntxtvalue.Text);
        if (!string.IsNullOrEmpty(hdntxtvalue.Text))
        {
            string uriSingleCateg = "api/MasterCategories/GetSingleCategory?relatedcategoryid=" + SNO + "";
            HttpResponseMessage response = client.GetAsync(uriSingleCateg).Result;
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                MasterCategories mCateg = response.Content.ReadAsAsync<MasterCategories>().Result;
                if (mCateg.LSTSUBCATEGORIES.Count > 0 && mCateg.LSTSUBCATEGORIES != null)
                {
                    rblCateg.DataSource = mCateg.LSTSUBCATEGORIES;
                    rblCateg.DataBind();

                }
            }
        }
    }
    protected void btnSelectedFilter_Click(object sender, EventArgs e)
    {
        long SNO = Convert.ToInt64(hdntxtvalue2.Text);
        if (!string.IsNullOrEmpty(hdntxtvalue2.Text))
        {
            string uriSingleFilter = "api/MasterFilters/GetFilterByItsFilterID?filterID=" + SNO + "";
            HttpResponseMessage response = client.GetAsync(uriSingleFilter).Result;
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                MasterFilters mFilter = response.Content.ReadAsAsync<MasterFilters>().Result;

                if (mFilter.LSTSUBFILTERS.Count > 0 && mFilter.LSTSUBFILTERS != null)
                {
                    chkFilters.DataSource = mFilter.LSTSUBFILTERS;
                    chkFilters.DataBind();
                }
            }
        }
    }
    protected void btnSaveLink_Click(object sender, EventArgs e)
    {
        foreach (ListItem item in chkFilters.Items)
        {
            if (item.Selected)
            {
                long SNO = Convert.ToInt64(item.Value);
                MasterCategories mCateg = App.myFashionsDBContext.MasterCategories.ToList().Where(c => c.SNO.Equals(SNO)).FirstOrDefault();
            }
        }
    }
}