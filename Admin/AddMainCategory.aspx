﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="AddMainCategory.aspx.cs" Inherits="Admin_AddMainCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ol class="breadcrumb">
        <li><a href="AdminHome.aspx">Home</a></li>
        <li>Main Category</li>
        <li class="active">Add Main Category</li>
    </ol>

    <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="row form-group">
            <div class="col-md-4 col-sm-12">
                <div class="lblSideHeading">Main Category Name</div>
            </div>
            <div class="col-md-6 col-sm-12">
                <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:TextBox ID="txtMainCategoryName" runat="server" class="form-control txt-uppercase maincata" MaxLength="30" placeholder="Main Category Name" />
                        <asp:Label ID="txtexist" runat="server" class="error-code maincataerrfroms"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <span class="mainerrorfiled error-code"></span>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4 col-sm-12">
                <div class="lblSideHeading">Choose file</div>
            </div>
            <div class="col-md-6 col-sm-12">
                <asp:FileUpload ID="fuMainCategoryImg" runat="server" CssClass="imageupload form-control"  
                    data-toggle="tooltip" data-placement="bottom" title="select only '.png' format" />
                <asp:HiddenField ID="hdGuid" runat="server" />
                <span class="imgerror error-code"></span>
            </div>
        </div>        
        <div class="row form-group">
            <div class="col-md-4 col-sm-12">
            </div>
            <div class="col-md-6 col-sm-12" style="padding-left:32px;">
                <asp:CheckBox ID="chkActive" runat="server" CssClass="checkbox" Checked="true" Text="Is Active" />
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4 col-sm-12">
            </div>
            <div class="col-md-6 col-sm-12" style="margin-left: 15px;">
                <div class="row form-group">
                    <asp:Button ID="btnSubmit" class="btn btn-success savebtn" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnClear" runat="server" Text="Clear" class="btn btn-default" OnClick="btnClear_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

