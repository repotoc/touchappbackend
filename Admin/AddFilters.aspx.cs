﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AddFilters : System.Web.UI.Page
{
    DropDownList ddlMain;
    object lockTarget = new object();
    string sGuid;
    string frameURI;
    HttpClient client;
    List<Stores> lstNewStores = new List<Stores>();
    public static Stores store;
    string companyadminid;
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;
        companyadminid = Session["companyadminid"].ToString();
        if (!IsPostBack)
        {
            sGuid = Guid.NewGuid().ToString();
            hdGuid.Value = sGuid;
            string UriStore = "api/StoreLogin/GetAllStores?relatedcompanyid=" + companyadminid;
            HttpResponseMessage responseStore = client.GetAsync(UriStore).Result;
            if (responseStore.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                List<Stores> selectedStores = responseStore.Content.ReadAsAsync<List<Stores>>().Result;
                chkStores.DataSource = selectedStores.Where(c => c.ISACTIVE).ToList();
                chkStores.DataTextField = "Name";
                chkStores.DataValueField = "ID";
                chkStores.DataBind();
            }
            else
            {
                string errorMsg = responseStore.Content.ReadAsStringAsync().Result;
            }
        }
        string str = Session["companyadminid"].ToString();
        long strusername = Convert.ToInt64(Session["companyadminid"].ToString());
        string uri = "api/MasterFilters/GetAllFiltersUnderCompany?relatedcompanyid=" + strusername;
        HttpResponseMessage response = client.GetAsync(uri).Result;
        if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
        {
            List<MasterFilters> masFilters= response.Content.ReadAsAsync<List<MasterFilters>>().Result;
            if (masFilters.Count > 0)
            {
                noRecord.Visible = false;
                ddlMain = new DropDownList();
                ddlMain.ID = "ddlMain";
                ddlMain.AutoPostBack = true;
                ddlMain.DataTextField = "NAME";
                ddlMain.DataValueField = "SNO";
                ddlMain.CssClass = "slctdvalue form-control";
                ddlMain.DataSource = masFilters;
                ddlMain.DataBind();
                ddlMain.Items.Insert(0, "Select");
                PlaceHolder1.Controls.Add(ddlMain);
            }
            else
            {
                string errorMsg = response.Content.ReadAsStringAsync().Result;
            }
        }
        else
        {
            noRecord.Visible = true;
        }
    }

    protected void hdntxtvalue_TextChanged(object sender, EventArgs e)
    {
        TextBox selectedText = ((TextBox)sender);
        if (selectedText != null)
        {
            if (selectedText.Text != "Select")
            {
                long selectCategId = Convert.ToInt64(selectedText.Text);
                string uriSingleFilter = "api/MasterFilters/GetFilterByItsFilterID?filterID=" + selectCategId + "";
                HttpResponseMessage response = client.GetAsync(uriSingleFilter).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    MasterFilters selectedMainFilter = response.Content.ReadAsAsync<MasterFilters>().Result;
                    if (selectedMainFilter.LSTSUBFILTERS != null && selectedMainFilter.LSTSUBFILTERS.Count > 0)
                    {
                        int cnt = FindOccurence("ddl");
                        long selectedddlcnt = cnt + 1;
                        BuildDynamicDropDown(selectedMainFilter.LSTSUBFILTERS, selectedddlcnt);
                    }
                }
            }
        }
    }
    private int FindOccurence(string substr)
    {
        string reqstr = Request.Form.ToString();
        return ((reqstr.Length - reqstr.Replace(substr, "").Length) / substr.Length);
    }
    public static int i = 0;
    private void BuildDynamicDropDown(IList<MasterFilters> list, long uniqueID)
    {
        DropDownList ddl = new DropDownList();
        ddl.ID = "ddl" + uniqueID;
        ddl.DataSource = list;
        ddl.DataTextField = "NAME";
        ddl.DataValueField = "SNO";
        ddl.CssClass = "slctdvalue1 form-control";
        ddl.EnableViewState = true;
        ddl.DataBind();
        ddl.Items.Insert(0, new ListItem("Select", "Select", true));
        PlaceHolder2.Controls.Add(ddl);
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string val = hdntxtvalue.Text;
        if (!string.IsNullOrEmpty(val))
        {
            try
            {
                MasterFilters filter = new MasterFilters();
                filter.NAME = txtFilterName.Text.ToUpper();
                if (fuImage.HasFile)
                {
                    string fileName = Path.GetFileName(fuImage.PostedFile.FileName);
                    string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtFilterName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                    string path = @"D:\Client Management Component\Users\TouchBackEndImages1";
                    string folder = path + @"\MasterFilter";
                    string ImagesFolder = "MasterFilter";
                    string ServerPath = Server.MapPath("~/MasterFilter");
                    if (!Directory.Exists(ServerPath))
                    {
                        Directory.CreateDirectory(ServerPath);
                    }
                    fuImage.PostedFile.SaveAs(Path.Combine(ServerPath, hdGuid.Value + "~" + strRandomNumber));

                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    fuImage.PostedFile.SaveAs(Path.Combine(folder, hdGuid.Value + "~" + strRandomNumber));
                    filter.IMAGENAME = hdGuid.Value + "~" + strRandomNumber;
                    filter.IMAGELOCATION = "pack://siteoforigin:,,,/" + ImagesFolder + "/" + hdGuid.Value + "~" + strRandomNumber;
                    filter.WEBIMAGELOCATION = folder + hdGuid.Value + "~" + strRandomNumber;
                }
                filter.VERSION = 1;
                filter.LASTUPDATEDTIME = DateTime.Now;
                filter.ISACTIVE = true;
               
                long companyadminid = Convert.ToInt64(Session["companyadminid"].ToString());
                string uriCompany = "api/CompanyLogin/GetCompanyInfo?companyID=" + companyadminid + "";
                HttpResponseMessage responseCmp = client.GetAsync(uriCompany).Result;
                if (responseCmp.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    Company selectedCompany = responseCmp.Content.ReadAsAsync<Company>().Result;
                    filter.REGCOMPANY = selectedCompany;
                }
                else
                {
                    string errorMsg = responseCmp.Content.ReadAsStringAsync().Result;
                }
                List<string> lstStores = new List<string>();
                foreach (ListItem item in chkStores.Items)
                {
                    if (item.Selected)
                    {
                        long id = Convert.ToInt64(item.Value);
                        string uriStore = "api/StoreLogin/GetStoreInfo?relatedstoreid=" + id;
                        HttpResponseMessage responseStore = client.GetAsync(uriStore).Result;
                        if (responseStore.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                        {
                            store = new Stores();
                            store = responseStore.Content.ReadAsAsync<Stores>().Result;
                        }
                        else
                        {
                            string errorMsg = responseStore.Content.ReadAsStringAsync().Result;
                        }
                        lstNewStores.Add(store);
                    }
                }
                filter.LSTREGSTORE = lstNewStores;
                //long selectCategId = Convert.ToInt64(val);
                long selectCategId = 1;
                string uriSingleFilter = "api/MasterFilters/GetFilterByItsFilterID?filterID=" + selectCategId + "";
                HttpResponseMessage response = client.GetAsync(uriSingleFilter).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    MasterFilters selectedMainFilter = response.Content.ReadAsAsync<MasterFilters>().Result;
                    if (selectedMainFilter.LSTSUBFILTERS != null)
                    {
                        selectedMainFilter.LSTSUBFILTERS.Add(filter);
                    }
                    else
                    {
                        selectedMainFilter.LSTSUBFILTERS = new List<MasterFilters>();
                        selectedMainFilter.LSTSUBFILTERS.Add(filter);
                    }
                }
                else
                {
                    string errorMsg = response.Content.ReadAsStringAsync().Result;
                }
                string uriCreateFilter = "api/MasterFilters/MasterFiltersCRUDOperation?parameter=C";
                HttpResponseMessage responseCreateFilter = client.PostAsJsonAsync<MasterFilters>(uriCreateFilter, filter).Result;
                if (responseCreateFilter.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Filter Created Successfully');", true);
                }
                else
                {
                    string errorMsg = responseCreateFilter.Content.ReadAsStringAsync().Result;
                }
                ClearAll();
            }
            catch (Exception ex)
            {
            }
        }
    }
    private void ClearAll()
    {
        hdntxtvalue.Text = string.Empty;
        txtFilterName.Text = string.Empty;
        ddlMain.ClearSelection();
    }
}