﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyfashionsDB.Models;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

public partial class Admin_AddNewStore : System.Web.UI.Page
{
    object lockTarget = new object();
    string sGuid;
    string frameURI;
    HttpClient client;
    public static Stores store;
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;
        if (!IsPostBack)
        {
            if (Request.QueryString["ID"] != null)
            {
                LoadStoreEditData();
            }
            else
            {
                App.Logger.Info("Starts PageLoad in Add New Store Page");
                sGuid = Guid.NewGuid().ToString();
                hdGuid.Value = sGuid;
            }
        }
    }
    private void LoadStoreEditData()
    {
        long ID = Convert.ToInt64(Request.QueryString["ID"]);        
        string uri = "api/StoreLogin/GetStoreInfo?relatedstoreid=" + ID + "";
        HttpResponseMessage response = client.GetAsync(uri).Result;
        if (response.StatusCode.Equals(HttpStatusCode.OK))
        {
            store = response.Content.ReadAsAsync<Stores>().Result;
            if (store != null)
            {
                btnSave.Text = "Update";
                btnCancel.Text = "Reset";
                txtstoreId.Text = store.STOREID;
                txcompanyName.Text = store.NAME;
                txtusername.Attributes.Add("readonly", "true");
                txtusername.Text = store.USERNAME;
                txtPassword.Text = store.PASSWORD;
                txtCity.Text = store.CITY;
                txtSate.Text = store.STATE;
                txtadd1.Text = store.ADDRES1;
                txtadd2.Text = store.ADDRESS2;
                txtnoemp.Text = store.NUMEMPLOYEES.ToString();
                if (store.ISACTIVE)
                    chkActive.Checked = true;
                else
                    chkActive.Checked = false;
            }
        }
        else
        {
            string errorMsg = response.Content.ReadAsStringAsync().Result;
            ClientScript.RegisterStartupScript(GetType(), "notifier13", "msgAlert('" + errorMsg + "');", true);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (btnSave.Text == "Save")
            {
                store = new Stores();
                store.STOREID = txtstoreId.Text.ToUpper();
                store.NAME = txcompanyName.Text.Trim().ToUpper();
                store.USERNAME = txtusername.Text.Trim().ToUpper();
                store.PASSWORD = txtPassword.Text.Trim();
                store.CITY = txtCity.Text.Trim().ToUpper();
                store.STATE = txtSate.Text.Trim().ToUpper();
                store.ADDRES1 = txtadd1.Text.Trim().ToUpper();
                store.ADDRESS2 = txtadd2.Text.Trim().ToUpper();
                store.NUMEMPLOYEES = Convert.ToInt32(txtnoemp.Text);
                if (fplogo.HasFile)
                {
                    string fileName = Path.GetFileName(fplogo.PostedFile.FileName);
                    string strRandomNumber = new Random().Next(1000, 100000).ToString() + txcompanyName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                    string path = @"D:\Client Management Component\Users\TouchBackEndImages1";
                    string folder = path + @"\StoreLogo";
                    string ImagesFolder = "StoreLogo";

                    string ServerPath = Server.MapPath("~/Admin/StoreLogo");
                    if (!Directory.Exists(ServerPath))
                    {
                        Directory.CreateDirectory(ServerPath);
                    }
                    fplogo.PostedFile.SaveAs(Path.Combine(ServerPath, hdGuid.Value + "~" + strRandomNumber));

                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    fplogo.PostedFile.SaveAs(Path.Combine(folder, hdGuid.Value + "~" + strRandomNumber));
                    store.IMAGENAME = hdGuid.Value + "~" + strRandomNumber;
                    store.IMAGELOCATION = "pack://siteoforigin:,,,/" + ImagesFolder + "/" + hdGuid.Value + "~" + strRandomNumber;
                    store.WEBIMAGELOCATION = folder + hdGuid.Value + "~" + strRandomNumber;
                }

                store.VERSION = 1;
                if (chkActive.Checked)
                {
                    store.ISACTIVE = true;
                }
                else
                {
                    store.ISACTIVE = false;
                }
                store.LASTUPDATEDTIME = DateTime.Now;
                string strcmpid = Session["companyadminid"].ToString();
                string uricmp = "api/CompanyLogin/GetCompanyInfo?companyID=" + strcmpid + "";
                HttpResponseMessage responsecmp = client.GetAsync(uricmp).Result;
                if (responsecmp.StatusCode.Equals(HttpStatusCode.OK))
                {
                    store.REGISTEREDCOMPANY = responsecmp.Content.ReadAsAsync<Company>().Result;
                }
                else
                {
                    string errorMsg = responsecmp.Content.ReadAsStringAsync().Result;
                }
                #region CrudOperation
                string uric = "api/StoreLogin/StoreCRUDOperation?parameter=C";
                HttpResponseMessage responsec = client.PostAsJsonAsync<Stores>(uric, store).Result;
                if (responsec.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    string strnew = Newtonsoft.Json.JsonConvert.SerializeObject(store);
                    ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Store Created Successfully');", true);
                }
                else
                {
                    string errorMsg = responsec.Content.ReadAsStringAsync().Result;
                }
                #endregion

            }
            else if (btnSave.Text == "Update")
            {
                    store.STOREID = txtstoreId.Text;
                    store.NAME = txcompanyName.Text.Trim().ToUpper();
                    store.USERNAME = txtusername.Text.Trim().ToUpper();
                    store.PASSWORD = txtPassword.Text.Trim();
                    store.CITY = txtCity.Text.Trim().ToUpper();
                    store.STATE = txtSate.Text.Trim().ToUpper();
                    store.ADDRES1 = txtadd1.Text.Trim().ToUpper();
                    store.ADDRESS2 = txtadd2.Text.Trim().ToUpper();
                    store.NUMEMPLOYEES = Convert.ToInt32(txtnoemp.Text);
                    if (fplogo.HasFile)
                    {
                        string fileName = Path.GetFileName(fplogo.PostedFile.FileName);
                        string strRandomNumber = new Random().Next(1000, 100000).ToString() + txcompanyName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                        string path = @"D:\Client Management Component\Users\TouchBackEndImages1";
                        string folder = path + @"\StoreLogo";
                        string ImagesFolder = "StoreLogo";

                        string ServerPath = Server.MapPath("~/Admin/StoreLogo");
                        if (!Directory.Exists(ServerPath))
                        {
                            Directory.CreateDirectory(ServerPath);
                        }
                        fplogo.PostedFile.SaveAs(Path.Combine(ServerPath, hdGuid.Value + "~" + strRandomNumber));

                        if (!Directory.Exists(folder))
                        {
                            Directory.CreateDirectory(folder);
                        }
                        fplogo.PostedFile.SaveAs(Path.Combine(folder, hdGuid.Value + "~" + strRandomNumber));
                        store.IMAGENAME = hdGuid.Value + "~" + strRandomNumber;
                        store.IMAGELOCATION = "pack://siteoforigin:,,,/" + ImagesFolder + "/" + hdGuid.Value + "~" + strRandomNumber;
                        store.WEBIMAGELOCATION = folder + hdGuid.Value + "~" + strRandomNumber;
                    }
                    store.VERSION = store.VERSION + 1;
                    if (chkActive.Checked)
                    {
                        store.ISACTIVE = true;
                    }
                    else
                    {
                        store.ISACTIVE = false;
                    }
                    store.LASTUPDATEDTIME = DateTime.Now;
                    string strcmpid = Session["companyadminid"].ToString();
                    string uricmp = "api/CompanyLogin/GetCompanyInfo?companyID=" + strcmpid + "";
                    HttpResponseMessage responsecmp = client.GetAsync(uricmp).Result;
                    if (responsecmp.StatusCode.Equals(HttpStatusCode.OK))
                    {
                        store.REGISTEREDCOMPANY = responsecmp.Content.ReadAsAsync<Company>().Result;
                    }
                    else
                    {
                        string errorMsg = responsecmp.Content.ReadAsStringAsync().Result;
                    }
                    string uri = "api/StoreLogin/StoreCRUDOperation?parameter=U";
                    HttpResponseMessage response = client.PostAsJsonAsync<Stores>(uri, store).Result;
                    if (response.StatusCode.Equals(HttpStatusCode.OK))
                    {
                        string strnew = Newtonsoft.Json.JsonConvert.SerializeObject(store);
                        ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Store Updated Successfully');setInterval(function(){window.location='ViewStores.aspx';},1000);", true);
                    }
                    else
                    {
                        string errorMsg = response.Content.ReadAsStringAsync().Result;
                    }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (btnCancel.Text == "Cancel")
        {
            clearAll();
        }
        else if (btnCancel.Text == "Reset")
        {
            LoadStoreEditData();
        }
    }

    private void clearAll()
    {
        txtstoreId.Text = string.Empty;
        txcompanyName.Text = string.Empty;
        txtusername.Text = string.Empty;
        txtCity.Text = string.Empty;
        txtSate.Text = string.Empty;
        txtadd1.Text = string.Empty;
        txtadd2.Text = string.Empty;
        txtnoemp.Text = string.Empty;
    }
}