﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using MyfashionsDB.Models;
using MyfashionsDB.Constants;
using System.Web.UI.WebControls;
using System.IO;
using System.Net;

public partial class Admin_MasterOffers : System.Web.UI.Page
{
    string frameURI;
    HttpClient client;
    long companyadminid;
    DropDownList ddlMain;
    List<Stores> lstNewStores = new List<Stores>();
    public static List<Int64> lstVal = new List<Int64>();
    //public List<Stores> selectedStores=new List<Stores>();
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;
        companyadminid = Convert.ToInt64(Session["companyadminid"].ToString());
        if (!IsPostBack)
        {
            string UriStore = "api/StoreLogin/GetAllStores?relatedcompanyid=" + companyadminid + "";
            HttpResponseMessage responseStore = client.GetAsync(UriStore).Result;
            if (responseStore.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                List<Stores> selectedStores = responseStore.Content.ReadAsAsync<List<Stores>>().Result;
                chkStores.DataSource = selectedStores.Where(c=>c.ISACTIVE).ToList();
                chkStores.DataTextField = "Name";
                chkStores.DataValueField = "ID";
                chkStores.DataBind();
            }
            else
            {
                string errorMsg = responseStore.Content.ReadAsStringAsync().Result;
            }
        }
        #region ddlMainBinding
        ddlMain = new DropDownList();
        ddlMain.ID = "ddlMain";
        ddlMain.AutoPostBack = true;
        ddlMain.DataTextField = "NAME";
        ddlMain.DataValueField = "SNO";
        ddlMain.CssClass = "slctdvalue form-control";
        try
        {
            string str = Session["companyadminid"].ToString();
            long strusername = Convert.ToInt64(Session["companyadminid"].ToString());
            string uri = "api/MasterCategories/GetAllCategories?relatedcompanyid=" + strusername + "&fetchTopCategories=true";
            HttpResponseMessage response = client.GetAsync(uri).Result;
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                MainEnum main = response.Content.ReadAsAsync<MainEnum>().Result;
                ddlMain.DataSource = main.LSTCATEGORIES;
                ddlMain.DataBind();
                ddlMain.Items.Insert(0, "Select");
                //ddlMain.SelectedIndexChanged += new EventHandler(Dynamic_Method);
                PlaceHolder1.Controls.Add(ddlMain);
            }
            else
            {
                string errorMsg = response.Content.ReadAsStringAsync().Result;
            }
        }
        catch (Exception ex)
        {
        }
        #endregion
    }
    private void Dynamic_Method(object sender, EventArgs e)
    {
        if (ddlMain.SelectedItem.Text == "Select")
        {
            chkCateg.Items.Clear();
            chkCateg.DataSource = null;
            chkCateg.DataBind();
        }
    }
    protected void ddlDiscountType_SelectedIndexChanged(object sender, EventArgs e)
    {
        divBuyGet.Visible = false;
        divRPC.Visible = false;
        lblType.Text = "";
        if (ddlDiscountType.SelectedIndex > 0)
        {
            if (ddlDiscountType.SelectedValue.Equals("B"))
            {
                divBuyGet.Visible = true;
            }
            else if (ddlDiscountType.SelectedValue.Equals("R"))
            {
                divRPC.Visible = true;
                lblType.Text = "Rs";
            }
            else if (ddlDiscountType.SelectedValue.Equals("P"))
            {
                divRPC.Visible = true;
                lblType.Text = "%";
            }
            else if (ddlDiscountType.SelectedValue.Equals("C"))
            {
                divRPC.Visible = true;
            }
            else
            {
                divBuyGet.Visible = false;
                divRPC.Visible = false;
            }
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            MasterOffers masterOffer = new MasterOffers();
            lstVal.ForEach(c =>
            {
                string uriSingleCateg = "api/MasterCategories/GetSingleCategory?relatedcategoryid=" + c + "";
                HttpResponseMessage response = client.GetAsync(uriSingleCateg).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    MasterCategories selectedMainCategory = response.Content.ReadAsAsync<MasterCategories>().Result;
                    if (masterOffer.LSTAPPLIEDSUBCATEGORIES != null)
                    {
                        masterOffer.LSTAPPLIEDSUBCATEGORIES.Add(selectedMainCategory);
                    }
                    else
                    {
                        masterOffer.LSTAPPLIEDSUBCATEGORIES = new List<MasterCategories>();
                        masterOffer.LSTAPPLIEDSUBCATEGORIES.Add(selectedMainCategory);
                    }
                }
                else
                {
                    string errorMsg = response.Content.ReadAsStringAsync().Result;
                }
            });
            masterOffer.NAME = txtOfferName.Text.ToUpper();
            masterOffer.DESCRIPTION = txtDescription.Text.Trim();
            if (fuOfferImage.HasFile)
            {
                string fileName = Path.GetFileName(fuOfferImage.PostedFile.FileName);
                string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtOfferName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                string path = @"D:\Client Management Component\Users\TouchBackEndImages1";
                string folder = path + @"\OfferImages";
                string ImagesFolder = "OfferImages";
                string ServerPath = Server.MapPath("~/OfferImages");
                if (!Directory.Exists(ServerPath))
                {
                    Directory.CreateDirectory(ServerPath);
                }
                fuOfferImage.PostedFile.SaveAs(Path.Combine(ServerPath, hdGuid.Value + "~" + strRandomNumber));

                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                fuOfferImage.PostedFile.SaveAs(Path.Combine(folder, hdGuid.Value + "~" + strRandomNumber));
                masterOffer.IMAGENAME = hdGuid.Value + "~" + strRandomNumber;
                masterOffer.IMAGELOCATION = "pack://siteoforigin:,,,/" + ImagesFolder + "/" + hdGuid.Value + "~" + strRandomNumber;
                masterOffer.WEBIMAGELOCATION = folder + hdGuid.Value + "~" + strRandomNumber;
            }
            if (ddlDiscountType.SelectedValue.Equals("B"))
            {
                masterOffer.BUYVALUE =Convert.ToInt32(txtBuyValue.Text);
                masterOffer.GETVALUE =Convert.ToInt32(txtGetValue.Text);
                masterOffer.DISCOUNTTYPE = "B";
                DiscountTypes.BuyGetOffer.Equals("B");
            }
            else if (ddlDiscountType.SelectedValue.Equals("P"))
            {
                masterOffer.DISCOUNT =Convert.ToDecimal(txtOfferValue.Text);
                masterOffer.DISCOUNTTYPE = "P";
                DiscountTypes.PercentageDiscount.Equals("P");
            }
            else if (ddlDiscountType.SelectedValue.Equals("R"))
            {
                masterOffer.DISCOUNT =Convert.ToDecimal(txtOfferValue.Text);
                masterOffer.DISCOUNTTYPE = "R";
                DiscountTypes.AmountDiscount.Equals("R");
            }
            else if(ddlDiscountType.SelectedValue.Equals("C"))
            {
                masterOffer.DISCOUNT =Convert.ToDecimal(txtOfferValue.Text);
                masterOffer.DISCOUNTTYPE = "C";
                DiscountTypes.Complimentory.Equals("C");
            }
            masterOffer.LASTUPDATEDTIME = DateTime.Now;
            masterOffer.VERSION = 1;
            Company selectedCompany=null;
            string uriCompany = "api/CompanyLogin/GetCompanyInfo?companyID=" + companyadminid + "";
            HttpResponseMessage responseCmp = client.GetAsync(uriCompany).Result;
            if (responseCmp.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                selectedCompany = responseCmp.Content.ReadAsAsync<Company>().Result;
            }
            else
            {
                string errorMsg = responseCmp.Content.ReadAsStringAsync().Result;
            }
            if (selectedCompany != null)
            {
                masterOffer.REGCOMPANY = selectedCompany;
            }
            if (chkexpired.Checked)
            {
                masterOffer.ISEXPIRED = true;
            }
            else
            {
                masterOffer.ISEXPIRED = false;
            }
            Stores storeItem=null;
            List<string> lstStores = new List<string>();
            foreach (ListItem item in chkStores.Items)
            {
                if (item.Selected)
                {
                    long id = Convert.ToInt64(item.Value);
                    string uriStore = "api/StoreLogin/GetStoreInfo?relatedstoreid=" + id;
                    HttpResponseMessage responseStore = client.GetAsync(uriStore).Result;
                    if (responseStore.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                    {
                        storeItem = new Stores();
                        storeItem = responseStore.Content.ReadAsAsync<Stores>().Result;
                    }
                    else
                    {
                        string errorMsg = responseStore.Content.ReadAsStringAsync().Result;
                    }
                    lstNewStores.Add(storeItem);
                }
            }
            masterOffer.LSTREGSTORE = lstNewStores;
            if (ddlDiscountType.SelectedValue.Equals("N"))
            {
                masterOffer.DISCOUNTTYPE = "N";
            }
            MainEnum mainenum = new MainEnum();
            mainenum.ID = Convert.ToInt64(MyfashionsDB.DBEnums.TopEnums.Offers);
            masterOffer.SELECTEDENUM = mainenum;
            string strnew = Newtonsoft.Json.JsonConvert.SerializeObject(masterOffer);
            string uriCreateOffer = "api/MasterOffers/MasterOffersCRUDOperation?parameter=C";
            HttpResponseMessage responseCreateCmp = client.PostAsJsonAsync<MasterOffers>(uriCreateOffer, masterOffer).Result;
            if (responseCreateCmp.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
               // strnew=  Newtonsoft.Json.JsonConvert.SerializeObject(masterOffer);
                ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Offer Created Successfully');setInterval(function(){window.location='../MasterOffers.aspx';},2000);", true);
            }
            else
            {
                //string strnew = Newtonsoft.Json.JsonConvert.SerializeObject(masterOffer);
                string errorMsg = responseCreateCmp.Content.ReadAsStringAsync().Result;
            }

        }
        catch (Exception ex)
        {
            
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
    protected void hdntxtvalue_TextChanged(object sender, EventArgs e)
    {
        TextBox selectedText = ((TextBox)sender);
        if (selectedText != null)
        {
            if (selectedText.Text != "Select")
            {
                long selectCategId = Convert.ToInt64(selectedText.Text);
                lstVal.Add(selectCategId);
                string uri = "api/MasterCategories/GetSubCategories?relatedmastercategoryid=" + selectCategId + "";
                HttpResponseMessage response = client.GetAsync(uri).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    List<MasterCategories> master = response.Content.ReadAsAsync<List<MasterCategories>>().Result;
                    int cnt = FindOccurence("ddl");
                    long selectedddlcnt = cnt + 1;
                    if (master.Count > 0)
                        BuildDynamicDropDown(master, selectedddlcnt);
                }
                else
                {
                    string errorMsg = response.Content.ReadAsStringAsync().Result;
                }
            }
        }
    }
    private int FindOccurence(string substr)
    {
        string reqstr = Request.Form.ToString();
        return ((reqstr.Length - reqstr.Replace(substr, "").Length) / substr.Length);
    }
    public static int i = 0;
    private void BuildDynamicDropDown(IList<MasterCategories> list, long uniqueID)
    {
        DropDownList ddl = new DropDownList();
        ddl.ID = "ddl" + uniqueID;
        ddl.DataSource = list;
        ddl.DataTextField = "NAME";
        ddl.DataValueField = "SNO";
        ddl.CssClass = "slctdvalue1 form-control mg-top";
        ddl.EnableViewState = true;
        ddl.DataBind();
        ddl.Items.Insert(0, new ListItem("Select", "Select", true));
        PlaceHolder2.Controls.Add(ddl);
    }
    protected void btnSelectedCateg_Click(object sender, EventArgs e)
    {
        long SNO = Convert.ToInt64(hdntxtvalue.Text);
        if (!string.IsNullOrEmpty(hdntxtvalue.Text))
        {
              string uriSingleCateg = "api/MasterCategories/GetSingleCategory?relatedcategoryid=" + SNO + "";
                HttpResponseMessage response = client.GetAsync(uriSingleCateg).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    MasterCategories mCateg = response.Content.ReadAsAsync<MasterCategories>().Result;
                    if (mCateg.LSTSUBCATEGORIES.Count > 0 && mCateg.LSTSUBCATEGORIES != null)
                    {
                        chkCateg.DataSource = mCateg.LSTSUBCATEGORIES;
                        chkCateg.DataBind();
                    }
                }
        }
    }
}