﻿using System;
using MyfashionsDB.Models;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Net.Http;
using System.Net.Http.Headers;

public partial class Admin_AddMasterFilters : System.Web.UI.Page
{
    DropDownList ddlMain;
    object lockTarget = new object();
    List<Stores> lstNewStores = new List<Stores>();
    string sGuid;
    string frameURI;
    HttpClient client;
    string companyadminid;
    public static Stores store;
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;
        companyadminid = Session["companyadminid"].ToString();
        if (!IsPostBack)
        {
            App.Logger.Info("Starts PageLoad in AddMasterFilters Page");
            sGuid = Guid.NewGuid().ToString();
            hdGuid.Value = sGuid;
            string UriStore = "api/StoreLogin/GetAllStores?relatedcompanyid=" + companyadminid + "";
            HttpResponseMessage responseStore = client.GetAsync(UriStore).Result;
            if (responseStore.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                List<Stores> selectedStores = responseStore.Content.ReadAsAsync<List<Stores>>().Result;
                chkStores.DataSource = selectedStores.Where(c => c.ISACTIVE).ToList();
                chkStores.DataTextField = "Name";
                chkStores.DataValueField = "ID";
                chkStores.DataBind();
            }
            else
            {
                string errorMsg = responseStore.Content.ReadAsStringAsync().Result;
            }
            //LoadData();
        }
    }

    private void LoadData()
    {
        DataTable dt = new DataTable();
        var Columns = new[]
            {
                new DataColumn("ID", typeof(long)),
                new DataColumn("NAME", typeof(string)),
                new DataColumn("LASTUPDATEDTIME", typeof(DateTime)),
            };

        dt.Columns.AddRange(Columns);
        long companyadminid = Convert.ToInt64(Session["companyadminid"].ToString());
          string uri = "api/MasterFilters/GetAllFiltersUnderCompany?relatedcompanyid=" + companyadminid;
            HttpResponseMessage responseCateg = client.GetAsync(uri).Result;
            if (responseCateg.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                List<MasterFilters> lstfilters = responseCateg.Content.ReadAsAsync<List<MasterFilters>>().Result;
              
                foreach (var item in lstfilters.OrderByDescending(c => c.LASTUPDATEDTIME))
                {
                    DataRow dtRow = dt.NewRow();
                    dtRow["ID"] = item.ID;
                    dtRow["NAME"] = item.NAME;
                    dtRow["LASTUPDATEDTIME"] = item.LASTUPDATEDTIME;
                    dt.Rows.Add(dtRow);
                }        
            }
   
        gvMasterFilters.DataSource = dt;
        gvMasterFilters.DataBind();
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            MasterFilters masterFilter = new MasterFilters();
            masterFilter.NAME = txtMasterFilterName.Text.Trim().ToUpper();
            if (fuMasterFilter.HasFile)
            {
                string fileName = Path.GetFileName(fuMasterFilter.PostedFile.FileName);
                string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtMasterFilterName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                string path = @"D:\Client Management Component\Users\TouchBackEndImages1";
                string folder = path + @"\MasterFilter";
                string ImagesFolder = "MasterFilter";

                string ServerPath = Server.MapPath("~/MasterFilter");
                if (!Directory.Exists(ServerPath))
                {
                    Directory.CreateDirectory(ServerPath);
                }
                fuMasterFilter.PostedFile.SaveAs(Path.Combine(ServerPath, hdGuid.Value + "~" + strRandomNumber));

                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                fuMasterFilter.PostedFile.SaveAs(Path.Combine(folder, hdGuid.Value + "~" + strRandomNumber));
                masterFilter.IMAGENAME = hdGuid.Value + "~" + strRandomNumber;
                masterFilter.IMAGELOCATION = "pack://siteoforigin:,,,/" + ImagesFolder + "/" + hdGuid.Value + "~" + strRandomNumber;
                masterFilter.WEBIMAGELOCATION = folder + hdGuid.Value + "~" + strRandomNumber;
            }
            long companyadminid = Convert.ToInt64(Session["companyadminid"].ToString());
            string uriCompany = "api/CompanyLogin/GetCompanyInfo?companyID=" + companyadminid;
            HttpResponseMessage responseCmp = client.GetAsync(uriCompany).Result;
            if (responseCmp.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                Company selectedCompany = responseCmp.Content.ReadAsAsync<Company>().Result;
                masterFilter.REGCOMPANY = selectedCompany;
            }
            else
            {
                string errorMsg = responseCmp.Content.ReadAsStringAsync().Result;
            }
            masterFilter.ISACTIVE = true;
            masterFilter.VERSION = 1;
            masterFilter.LASTUPDATEDTIME = DateTime.Now;
            //MainEnum mainEnum = new MainEnum();
            //if (mainEnum.LSTFILTERS != null)
            //{
            //    mainEnum.LSTFILTERS.Add(masterFilter);
            //}
            //else
            //{
            //    mainEnum.LSTFILTERS = new List<MasterFilters>();
            //    mainEnum.LSTFILTERS.Add(masterFilter);
            //}
            List<string> lstStores = new List<string>();
            foreach (ListItem item in chkStores.Items)
            {
                if (item.Selected)
                {
                    long id = Convert.ToInt64(item.Value);
                    string uriStore = "api/StoreLogin/GetStoreInfo?relatedstoreid=" + id;
                    HttpResponseMessage responseStore = client.GetAsync(uriStore).Result;
                    if (responseStore.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                    {
                        store = new Stores();
                        store = responseStore.Content.ReadAsAsync<Stores>().Result;
                    }
                    else
                    {
                        string errorMsg = responseStore.Content.ReadAsStringAsync().Result;
                    }
                    lstNewStores.Add(store);
                }
            }
            masterFilter.LSTREGSTORE = lstNewStores;
            MainEnum main = new MainEnum();
            main.ID =Convert.ToInt64(MyfashionsDB.DBEnums.TopEnums.Filters);
            masterFilter.SELECTEDENUM = main;
            string uriCreateFilter = "api/MasterFilters/MasterFiltersCRUDOperation?parameter=C";
            HttpResponseMessage responseCreateFilter = client.PostAsJsonAsync<MasterFilters>(uriCreateFilter, masterFilter).Result;
            if (responseCreateFilter.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Master Filter Created Successfully');", true);
            }
            else
            {
                string errorMsg = responseCreateFilter.Content.ReadAsStringAsync().Result;
            }
            LoadData();
            ClearAll();
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        ClearAll();
    }
    protected void gvMasterFilters_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "DELETE")
        {
            long ID = Convert.ToInt64(e.CommandArgument);
            string uri = "api/MasterFilters/GetFilterByItsFilterID?filterID"+ID;
            HttpResponseMessage responseGetFilter = client.GetAsync(uri).Result;
            if (responseGetFilter.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                MasterFilters selectedFilter = responseGetFilter.Content.ReadAsAsync<MasterFilters>().Result;
                string uriCreateFilter = "api/MasterFilters/MasterFiltersCRUDOperation?parameter=D";
                HttpResponseMessage responseCreateFilter = client.PostAsJsonAsync<MasterFilters>(uriCreateFilter, selectedFilter).Result;
                if (responseCreateFilter.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Master Filter Created Successfully');", true);
                }
                else
                {
                    string errorMsg = responseCreateFilter.Content.ReadAsStringAsync().Result;
                }
            }
            else
            {
                string errorMsg = responseGetFilter.Content.ReadAsStringAsync().Result;
            }
            LoadData();
            ClearAll();
        }
    }

    private void ClearAll()
    {
        txtMasterFilterName.Text = string.Empty;
    }
    protected void gvMasterFilters_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvMasterFilters_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvMasterFilters.PageIndex = e.NewPageIndex;
        LoadData();
        //gvMasterFilters.DataBind();
    }
}