﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="MasterCategMasterFilter.aspx.cs" Inherits="Admin_MasterCategMasterFilter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  <script>
      $(document).ready(function () {
          
          //CHECKING OF LOCALSTORAGE AND PREPENDING OF SELECT BOXES
          if (localStorage.getItem("htmldata0") != " " && localStorage.getItem("htmldata0") !== "undefined") {
              for (i = 0; i <= parseInt(dc) ; i++) {
                  var htmldata = localStorage.getItem("htmldata" + i + "");
                  $(".dsata").append("<select class='slctdvalue1 form-control mg-top' name=" + i + ">" + htmldata + "</select>");
              }
              //SETTING OF VALUES FOR DROPDOWN BY USING LOACLSTORAGE
              for (i = 0; i <= parseInt(dc) ; i++) {
                  var a = localStorage.getItem("dfltdata" + i + "");
                  $(".slctdvalue1 option").each(function (e, i) {
                      var b = $(this).val();
                      if (a == b) {
                          $(this).parent().val(a);
                      }
                  });
              }
          }

          //REST OF TEXTBOX CHANGE FUNCTION AND CHANGE FUNCTION
          //$(document).on("change", ".slctdvalue", function () {
          //    $(".dsata").html("");
          //});

          //REST OF TEXTBOX CHANGE FUNCTION AND CHANGE FUNCTION
          $(document).on("change", ".slctdvalue,.slctdvalue1", function () {
              $(".hdnvalue1").val($(this).val());
              $(".hdnvalue1").change();          
              $(".slctdvalue1").each(function (e, i) {
                  localStorage.setItem("htmldata" + e + "", $(this).html());
                  localStorage.setItem("dfltdata" + e + "", $(this).val());
                  localStorage.setItem("noof", e);
              });
          });


            //CHECKING OF LOCALSTORAGE AND PREPENDING OF SELECT BOXES          
            if (localStorage.getItem("htmldata20") != " " && localStorage.getItem("htmldata20") !== "undefined") {
                for (i = 0; i <= parseInt(dc2) ; i++) {
                    var htmldata2 = localStorage.getItem("htmldata2" + i + "");
                    $(".dsata2").append("<select class='slctdvalue4 form-control mg-top' name=" + i + ">" + htmldata2 + "</select>");
                }
                //SETTING OF VALUES FOR DROPDOWN BY USING LOACLSTORAGE
                for (i = 0; i <= parseInt(dc2) ; i++) {
                    var a = localStorage.getItem("dfltdata2" + i + "");
                    $(".slctdvalue4 option").each(function (e, i) {
                        var b = $(this).val();
                        if (a == b) {
                            $(this).parent().val(a);
                        }
                    });
                }
            }
          
            //REST OF TEXTBOX CHANGE FUNCTION AND CHANGE FUNCTION
            $(document).on("change", ".slctdvalue3,.slctdvalue4", function () {
                $(".hdnvalue2").val($(this).val());
                $(".hdnvalue2").change();
                $(".slctdvalue4").each(function (e, i) {
                    localStorage.setItem("htmldata2" + e + "", $(this).html());
                    localStorage.setItem("dfltdata2" + e + "", $(this).val());
                    localStorage.setItem("noof2", e);
                });
            })
        })
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <ol class="breadcrumb">
        <li><a href="AdminHome.aspx">Home</a></li>
        <li>Linking</li>
        <li class="active">Master Category - Filters</li>
    </ol>
    <div class="row form-group">
        <div class="col-sm-5 col-md-5 form-group" style="border-right: 1px solid #E6ECF7;">
            <asp:PlaceHolder ID="PlaceHolder1" runat="server" />
            <div class="dsata" id="divCateg" runat="server"></div>
            <asp:PlaceHolder ID="PlaceHolder2" runat="server"/>
            <asp:TextBox ID="hdntxtvalue" runat="server" OnTextChanged="hdntxtvalue_TextChanged" AutoPostBack="true" CssClass="hdnvalue" Style="display: none;" />
            <asp:TextBox ID="hdnvaluedync" runat="server" CssClass="hdnvalue" Style="display: none;" />
            <asp:Button ID="btnSelectedCateg" runat="server" CssClass="btn btn-info btn-md btn-block mg-top" Text="Select Category Linking" OnClick="btnSelectedCateg_Click"/>
        </div>

         <div class="col-sm-1 col-md-1"><asp:Image id="imgLing" runat="server" ImageUrl="~/Icons/link.png" CssClass="img-responsive" Height="50px" Width="50px" /></div>

         <div class="col-sm-5 col-md-5 form-group" style="border-left: 1px solid #E6ECF7;">
            <asp:PlaceHolder ID="PlaceHolder3" runat="server" />
            <div class="dsata2"></div>
            <asp:PlaceHolder ID="PlaceHolder4" runat="server" />
            <asp:TextBox ID="hdntxtvalue2" runat="server" OnTextChanged="hdntxtvalue2_TextChanged" AutoPostBack="true" CssClass="hdnvalue2" Style="display: none;" />
            <asp:TextBox ID="hdnvaluedync1" runat="server" CssClass="hdnvalue3" Style="display: none;" />
            <asp:Button ID="btnSelectedFilter" runat="server" CssClass="btn btn-info btn-md btn-block mg-top" Text="Select Filter Linking" OnClick="btnSelectedFilter_Click"/>
        </div>
    </div>
    <div class="row">
        <hr class="hr-1" />
    </div>
    <div class="row">
        <div class="col-sm-5 col-md-5 form-group">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Selected Category List</div>
                <div class="panel-body" style="padding-left:35px;">
                    <asp:RadioButtonList ID="rblCateg" runat="server" TextAlign="Right" CssClass="radio" RepeatDirection="Vertical" DataTextField="NAME" DataValueField="SNO" />
                </div>
            </div>
        </div>
        <div class="col-sm-1 col-md-1"></div>

        <div class="col-sm-5 col-md-5 form-group">

            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Selected Filters List</div>
                <div class="panel-body" style="padding-left: 35px;">
                    <asp:CheckBoxList ID="chkFilters" runat="server" TextAlign="Right" CssClass="checkbox" RepeatDirection="Vertical" DataTextField="NAME" DataValueField="SNO" />
                </div>
            </div>
        </div>
    </div>
    <div class="row col-centered">
        <asp:Button ID="btnSaveLink" runat="server" Text="Save Link" CssClass="btn btn-success" OnClick="btnSaveLink_Click" />
    </div>
</asp:Content>

