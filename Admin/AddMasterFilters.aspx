﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="AddMasterFilters.aspx.cs" Inherits="Admin_AddMasterFilters" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ol class="breadcrumb">
        <li><a href="AdminHome.aspx">Home</a></li>
        <li>Filters</li>
        <li class="active">Add Master Filters</li>
    </ol>
    <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="row form-group">
                    <div class="col-md-4 col-sm-12">
                        <div class="lblSideHeading">Master Filter Name</div>
                    </div>
                    <div class="col-md-6 col-sm-12">

                        <asp:TextBox ID="txtMasterFilterName" runat="server" class="form-control txt-uppercase maincata" MaxLength="30" placeholder="Master Filter Name" />
                        <asp:Label ID="txtexist" runat="server" class="error-code maincataerrfroms"></asp:Label>

                        <span class="mainerrorfiled error-code"></span>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4 col-sm-12">
                        <div class="lblSideHeading">Choose file</div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                         <asp:FileUpload ID="fuMasterFilter" runat="server" CssClass="imageupload form-control"  
                    data-toggle="tooltip" data-placement="bottom" title="select only '.png' format" />
                        <asp:HiddenField ID="hdGuid" runat="server" />
                        <span class="imgerror error-code"></span>
                    </div>
                </div>
                 <div class="row form-group">
                <div class="col-sm-12 col-md-4"> <div class="lblSideHeading">Stores</div></div>
                <div class="col-sm-12 col-md-6">
                    <asp:CheckBoxList ID="chkStores" runat="server" CssClass="checkbox-1" Style="font-size: 12pt;" AppendDataBoundItems="true" AutoPostBack="true" SelectionMode="Multiple"></asp:CheckBoxList>
                </div>
            </div>
                <div class="row form-group">
                    <div class="col-md-4 col-sm-12">
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <asp:Image ID="lblImgLocation" runat="server" CssClass="requiredforimage img-thumbnail" />
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4 col-sm-12">
                    </div>
                    <div class="col-md-6 col-sm-12" style="margin-left: 15px;">
                        <div class="row form-group">
                            <asp:Button ID="btnSubmit" class="btn btn-success savebtn" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                            <asp:Button ID="btnClear" runat="server" Text="Clear" class="btn btn-default" OnClick="btnClear_Click" />
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="table-responsive">
                        <asp:GridView ID="gvMasterFilters" runat="server" AutoGenerateColumns="false"
                            DataKeyNames="ID" CssClass="table table-striped table-bordered" OnRowCommand="gvMasterFilters_RowCommand"
                            OnRowDeleting="gvMasterFilters_RowDeleting" AllowPaging="true" PageSize="6" OnPageIndexChanging="gvMasterFilters_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="ID">
                                    <ItemTemplate>
                                        <asp:Label ID="Label0" runat="server" Text='<%#Eval("ID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="NAME">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("NAME") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="LASTUPDATEDTIME">
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%#Eval("LASTUPDATEDTIME") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DELETE">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/Icons/delete.png" CommandName="DELETE" CommandArgument='<%#Eval("ID") %>' OnClientClick="return ConfirmDelete();" Style="height: 70%;" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="Pagination" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" />
                            <EmptyDataTemplate>
                                <ul id="noRecord" runat="server" class="list-group" style="text-align: center;">
                                    <li class="list-group-item">No Records Found</li>
                                </ul>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

