﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Default : System.Web.UI.Page
{
    string frameURI;
    HttpClient client; 
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            App.Logger.Info("Start of btnSubmit_Click in _Default page");
            string username = txtUserName.Text;
            string password = txtPassword.Text;
            string uri = "api/CompanyLogin/ValidateCompanyLogin?username="+ username + "&password=" + password + "";
            HttpResponseMessage response = client.GetAsync(uri).Result;
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                string str = response.Content.ReadAsStringAsync().Result;
                Session["companyadminusername"] = txtUserName.Text;
                Session["companyadminid"] = str;
                Response.Redirect("AdminHome.aspx");
            }
            else
            {
                string errorMsg = response.Content.ReadAsStringAsync().Result;
                ClientScript.RegisterStartupScript(GetType(), "notifier1", "errorAlert('Enter valid Username / Password');", true);
            }
            App.Logger.Info("End of btnSubmit_Click in _Default page");
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(GetType(), "error", "errorAlert('" + ex.Message + "');", true);
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
}