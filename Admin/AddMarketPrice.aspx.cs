﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AddMarketPrice : System.Web.UI.Page
{
    DropDownList ddlMain;
    object lockTarget = new object();
    string sGuid;
    string frameURI;
    HttpClient client;
    List<Stores> lstNewStores = new List<Stores>();
    public static Stores store;
    string companyadminid;
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;
        companyadminid = Session["companyadminid"].ToString();
        if (!IsPostBack)
        {
            sGuid = Guid.NewGuid().ToString();
        }
        string str = Session["companyadminid"].ToString();
        long strusername = Convert.ToInt64(Session["companyadminid"].ToString());
        string uri = "api/MasterFilters/GetAllFiltersUnderCompany?relatedcompanyid=" + strusername;
        HttpResponseMessage response = client.GetAsync(uri).Result;
        if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
        {
            List<MasterFilters> masFilters = response.Content.ReadAsAsync<List<MasterFilters>>().Result;
            if (masFilters.Count > 0)
            {
                noRecord.Visible = false;
                ddlMain = new DropDownList();
                ddlMain.ID = "ddlMain";
                ddlMain.AutoPostBack = true;
                ddlMain.DataTextField = "NAME";
                ddlMain.DataValueField = "ID";
                ddlMain.CssClass = "slctdvalue form-control";
                ddlMain.DataSource = masFilters;
                ddlMain.DataBind();
                ddlMain.Items.Insert(0, "Select");
                PlaceHolder1.Controls.Add(ddlMain);
            }
            else
            {
                string errorMsg = response.Content.ReadAsStringAsync().Result;
            }
        }
        else
        {
            noRecord.Visible = true;
        }
    }
    protected void hdntxtvalue_TextChanged(object sender, EventArgs e)
    {
        TextBox selectedText = ((TextBox)sender);
        if (selectedText != null)
        {
            if (selectedText.Text != "Select")
            {
                long selectCategId = Convert.ToInt64(selectedText.Text);
                string uriSingleFilter = "api/MasterFilters/GetFilterByItsFilterID?filterID=" + selectCategId + "";
                HttpResponseMessage response = client.GetAsync(uriSingleFilter).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    MasterFilters selectedMainFilter = response.Content.ReadAsAsync<MasterFilters>().Result;
                    if (selectedMainFilter.LSTSUBFILTERS != null && selectedMainFilter.LSTSUBFILTERS.Count > 0)
                    {
                        int cnt = FindOccurence("ddl");
                        long selectedddlcnt = cnt + 1;
                        BuildDynamicDropDown(selectedMainFilter.LSTSUBFILTERS, selectedddlcnt);
                    }
                }
            }
        }
    }
    private int FindOccurence(string substr)
    {
        string reqstr = Request.Form.ToString();
        return ((reqstr.Length - reqstr.Replace(substr, "").Length) / substr.Length);
    }
    public static int i = 0;
    private void BuildDynamicDropDown(IList<MasterFilters> list, long uniqueID)
    {
        DropDownList ddl = new DropDownList();
        ddl.ID = "ddl" + uniqueID;
        ddl.DataSource = list;
        ddl.DataTextField = "NAME";
        ddl.DataValueField = "ID";
        ddl.CssClass = "slctdvalue1 form-control";
        ddl.EnableViewState = true;
        ddl.DataBind();
        ddl.Items.Insert(0, new ListItem("Select", "Select", true));
        PlaceHolder2.Controls.Add(ddl);
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string val = hdntxtvalue.Text;
        if (!string.IsNullOrEmpty(val))
        {
            MarketPrice marketPrice = new MarketPrice();
            marketPrice.NOTATION =Convert.ToDecimal(txtNOtation.Text);
            marketPrice.NOTES = txtNotes.Text.ToUpper();
            marketPrice.PRICE =Convert.ToDecimal(txtPrice.Text);
            string uri = "api/MasterFilters/GetFilterByItsFilterID?filterID="+val;
            HttpResponseMessage responseGetFilter = client.GetAsync(uri).Result;
            if (responseGetFilter.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                MasterFilters selectedFilter = responseGetFilter.Content.ReadAsAsync<MasterFilters>().Result;
                if (selectedFilter.MARKETPRICE != null)
                {
                    selectedFilter.MARKETPRICE = marketPrice;
                }
                else
                {
                    selectedFilter.MARKETPRICE = new MarketPrice();
                    selectedFilter.MARKETPRICE = marketPrice;
                }
            }
            marketPrice.VERSION = 1;
            marketPrice.LASTUPDATEDTIME = DateTime.Now;
            string uriCreateMarketPrice = "api/MarketPrice/MarketPriceCRUDOperation?parameter=C";
            HttpResponseMessage responseCreatemarketPrice = client.PostAsJsonAsync<MarketPrice>(uriCreateMarketPrice, marketPrice).Result;
            if (responseCreatemarketPrice.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Market Price Created Successfully');", true);
            }
            else
            {
                string errorMsg = responseCreatemarketPrice.Content.ReadAsStringAsync().Result;
            }
        }
    }
}