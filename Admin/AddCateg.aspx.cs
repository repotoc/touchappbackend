﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AddCateg : System.Web.UI.Page
{
    DropDownList ddlMain;
    object lockTarget=new object();
    string sGuid;
    string frameURI;
    HttpClient client;
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;

        if (!IsPostBack)
        {
            sGuid = Guid.NewGuid().ToString();
            hdGuid.Value = sGuid;
        }
        ddlMain = new DropDownList();
        ddlMain.ID = "ddlMain";
        ddlMain.AutoPostBack = true;
        ddlMain.DataTextField = "NAME";
        ddlMain.DataValueField = "SNO";
        ddlMain.CssClass = "slctdvalue form-control";
        try
        {
            string str = Session["companyadminid"].ToString();
            long strusername = Convert.ToInt64(Session["companyadminid"].ToString());
            string uri = "api/MasterCategories/GetAllCategories?relatedcompanyid=" + strusername + "&fetchTopCategories=true";
            HttpResponseMessage response = client.GetAsync(uri).Result;
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                MainEnum main = response.Content.ReadAsAsync<MainEnum>().Result;
                ddlMain.DataSource = main.LSTCATEGORIES;
                ddlMain.DataBind();
                ddlMain.Items.Insert(0, "Select");
                //ddlMain.SelectedIndexChanged += new EventHandler(Dynamic_Method);
                PlaceHolder1.Controls.Add(ddlMain);
            }
            else
            {
                string errorMsg = response.Content.ReadAsStringAsync().Result;
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void hdntxtvalue_TextChanged(object sender, EventArgs e)
    {
        TextBox selectedText = ((TextBox)sender);
        if (selectedText != null)
        {
            if (selectedText.Text != "Select")
            {
                long selectCategId = Convert.ToInt64(selectedText.Text);

                string uri = "api/MasterCategories/GetSubCategories?relatedmastercategoryid=" + selectCategId + "";
                HttpResponseMessage response = client.GetAsync(uri).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    List<MasterCategories> master = response.Content.ReadAsAsync<List<MasterCategories>>().Result;
                    int cnt = FindOccurence("ddl");
                    long selectedddlcnt = cnt + 1;
                    if (master.Count > 0)
                        BuildDynamicDropDown(master, selectedddlcnt);
                }
                else
                {
                    string errorMsg = response.Content.ReadAsStringAsync().Result;
                }
            }
        }
    }
    private int FindOccurence(string substr)
    {
        string reqstr = Request.Form.ToString();
        return ((reqstr.Length - reqstr.Replace(substr, "").Length) / substr.Length);
    }
    public static int i = 0;
    private void BuildDynamicDropDown(IList<MasterCategories> list, long uniqueID)
    {
        DropDownList ddl = new DropDownList();
        ddl.ID = "ddl" + uniqueID;
        ddl.DataSource = list;
        ddl.DataTextField = "NAME";
        ddl.DataValueField = "SNO";
        ddl.CssClass = "slctdvalue1 form-control mg-top";
        ddl.EnableViewState = true;
        ddl.DataBind();
        ddl.Items.Insert(0, new ListItem("Select", "Select", true));
        PlaceHolder2.Controls.Add(ddl);
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string val = hdntxtvalue.Text;
        if (!string.IsNullOrEmpty(val))
        {
            try
            {
                MasterCategories mainCategory = new MasterCategories();
                mainCategory.NAME = txtCategoryName.Text.ToUpper();
                if (fuImage.HasFile)
                {
                    string fileName = Path.GetFileName(fuImage.PostedFile.FileName);
                    string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtCategoryName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                    string path = @"D:\Client Management Component\Users\TouchBackEndImages1";
                    string folder = path + @"\MainCategories";
                    string ImagesFolder = "MainCategories";
                    string ServerPath = Server.MapPath("~/MainCategories");
                    if (!Directory.Exists(ServerPath))
                    {
                        Directory.CreateDirectory(ServerPath);
                    }
                    fuImage.PostedFile.SaveAs(Path.Combine(ServerPath, hdGuid.Value + "~" + strRandomNumber));

                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    fuImage.PostedFile.SaveAs(Path.Combine(folder, hdGuid.Value + "~" + strRandomNumber));
                    mainCategory.IMAGENAME = hdGuid.Value + "~" + strRandomNumber;
                    mainCategory.IMAGELOCATION = "pack://siteoforigin:,,,/" + ImagesFolder + "/" + hdGuid.Value + "~" + strRandomNumber;
                    mainCategory.WEBIMAGELOCATION = folder + hdGuid.Value + "~" + strRandomNumber;
                }
                if (chkActive.Checked)
                    mainCategory.ISACTIVE = true;
                else
                    mainCategory.ISACTIVE = false;
                mainCategory.VERSION = 1;
                mainCategory.LASTUPDATEDTIME = DateTime.Now;

                long selectCategId = Convert.ToInt64(val);
                string uriSingleCateg = "api/MasterCategories/GetSingleCategory?relatedcategoryid=" + selectCategId + "";
                HttpResponseMessage response = client.GetAsync(uriSingleCateg).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    MasterCategories selectedMainCategory = response.Content.ReadAsAsync<MasterCategories>().Result;
                    mainCategory.SELECTEDCATEG = selectedMainCategory;
                }
                else
                {
                    string errorMsg = response.Content.ReadAsStringAsync().Result;
                }
                long companyadminid =Convert.ToInt64(Session["companyadminid"].ToString());
                string uriCompany = "api/CompanyLogin/GetCompanyInfo?companyID=" + companyadminid + "";
                HttpResponseMessage responseCmp = client.GetAsync(uriCompany).Result;
                if (responseCmp.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    Company selectedCompany = responseCmp.Content.ReadAsAsync<Company>().Result;
                    mainCategory.REGCOMPANY = selectedCompany;
                }
                else
                {
                    string errorMsg = responseCmp.Content.ReadAsStringAsync().Result;
                }

                string uriCreateCmp = "api/MasterCategories/MasterCategoriesCRUDOperation?parameter=C";
                HttpResponseMessage responseCreateCmp = client.PostAsJsonAsync<MasterCategories>(uriCreateCmp, mainCategory).Result;
                if (responseCreateCmp.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    string strnew = Newtonsoft.Json.JsonConvert.SerializeObject(mainCategory);
                    ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Category Created Successfully');", true);
                }
                else
                {
                    string errorMsg = responseCreateCmp.Content.ReadAsStringAsync().Result;
                }
                ClearAll();
            }
            catch (Exception ex)
            {
            }
        }
    }

    private void ClearAll()
    {
        hdntxtvalue.Text = string.Empty;
        txtCategoryName.Text = string.Empty;
        ddlMain.ClearSelection();
    }
}