﻿using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ViewMainCategory : System.Web.UI.Page
{
    object lockTarget = new object();    
    string frameURI;
    HttpClient client;
    public static MainEnum main;
    string errorMsg;
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;

        if (!IsPostBack)
        {
            LoadUpdate();

            try
            {
                long cID = Convert.ToInt64(Session["companyadminid"]);
                string uri = "api/MasterCategories/GetAllCategories?relatedcompanyid=" + cID;
                HttpResponseMessage response = client.GetAsync(uri).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    main = response.Content.ReadAsAsync<MainEnum>().Result;
                    lstMainCategory.DataSource = main.LSTCATEGORIES;
                    lstMainCategory.DataBind();
                }
                else
                {
                    string errorMsg = response.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(GetType(), "notifierLoadError", "errorAlert('" + ex.Message + "');", true);
            }
        }
    }
    protected void btnEditUpdate_Click(object sender, EventArgs e)
    {
        if (btnEditUpdate.Text == "Edit")
        {
            string ID = "1";
            if (!string.IsNullOrEmpty(ID))
                LoadEdit();
        }
        else if (btnEditUpdate.Text == "Update")
        {
            long ID = 1;
            string uriSingleCateg = "api/MasterCategories/GetSingleCategory?relatedcategoryid=" + ID + "";
            HttpResponseMessage response = client.GetAsync(uriSingleCateg).Result;
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                MasterCategories mCategory = response.Content.ReadAsAsync<MasterCategories>().Result;
                if (mCategory != null)
                {
                    mCategory.NAME = txtCategoryName.Text;
                    if (fuImage.HasFile)
                    {
                        string fileName = Path.GetFileName(fuImage.PostedFile.FileName);
                        string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtCategoryName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                        string path = @"D:\Client Management Component\Users\TouchBackEndImages1";
                        string folder = path + @"\MainCategories";
                        string ImagesFolder = "MainCategories";
                        string ServerPath = Server.MapPath("~/MainCategories");
                        if (!Directory.Exists(ServerPath))
                        {
                            Directory.CreateDirectory(ServerPath);
                        }
                        fuImage.PostedFile.SaveAs(Path.Combine(ServerPath, hdGuid.Value + "~" + strRandomNumber));

                        if (!Directory.Exists(folder))
                        {
                            Directory.CreateDirectory(folder);
                        }
                        fuImage.PostedFile.SaveAs(Path.Combine(folder, hdGuid.Value + "~" + strRandomNumber));
                        mCategory.IMAGENAME = hdGuid.Value + "~" + strRandomNumber;
                        mCategory.IMAGELOCATION = "pack://siteoforigin:,,,/" + ImagesFolder + "/" + hdGuid.Value + "~" + strRandomNumber;
                        mCategory.WEBIMAGELOCATION = folder + hdGuid.Value + "~" + strRandomNumber;
                    }
                    if (chkActive.Checked)
                        mCategory.ISACTIVE = true;
                    else
                        mCategory.ISACTIVE = false;
                    mCategory.REGCOMPANY = mCategory.REGCOMPANY;
                    mCategory.SELECTEDENUM = mCategory.SELECTEDENUM;
                    mCategory.VERSION = mCategory.VERSION + 1;
                    mCategory.LASTUPDATEDTIME = DateTime.Now;
                    string uri = "api/MasterCategories/MasterCategoriesCRUDOperation?parameter=U";
                    HttpResponseMessage responseU = client.PostAsJsonAsync<MasterCategories>(uri, mCategory).Result;
                    if (responseU.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Updated Successfully');", true);
                    }
                    else
                    {
                        string s = Newtonsoft.Json.JsonConvert.SerializeObject(mCategory);
                        errorMsg = responseU.Content.ReadAsStringAsync().Result;
                    }
                }
            }
            else
            {
                errorMsg = response.Content.ReadAsStringAsync().Result;
            }

        }
    }
    private void LoadEdit()
    {
        divFUpload.Visible = true;
        divImage.Visible = false;
        txtCategoryName.Attributes.Remove("readonly");
        btnEditUpdate.Text = "Update";
    }
    private void LoadUpdate()
    {
        divFUpload.Visible = false;
        divImage.Visible = true;
        txtCategoryName.Attributes.Add("readonly", "true");
        btnEditUpdate.Text = "Edit";
    }
    protected void lstMainCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadUpdate();
        divEditPanel.Visible = true;
        string ST = lstMainCategory.SelectedItem.Text;
        long ID = Convert.ToInt64(lstMainCategory.SelectedItem.Value);
        MasterCategories mCategory = main.LSTCATEGORIES.ToList().Where(c => c.ID.Equals(ID)).FirstOrDefault();
        txtCategoryName.Text = mCategory.NAME;
        imgCtg.ImageUrl = @"~/MainCategories/" + mCategory.IMAGENAME;
    }
}