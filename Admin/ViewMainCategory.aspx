﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ViewMainCategory.aspx.cs" Inherits="Admin_ViewMainCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ol class="breadcrumb">
        <li><a href="AdminHome.aspx">Home</a></li>
        <li>Main Category</li>
        <li class="active">View Main Category</li>
    </ol>

    <div class="col-xs-12 col-md-12 col-sm-12">

        <div class="col-xs-6 col-md-6 col-sm-6 form-group">
            <div class="row form-group table-responsive">
                <asp:ListBox ID="lstMainCategory" runat="server" AutoPostBack="true"
                    OnSelectedIndexChanged="lstMainCategory_SelectedIndexChanged" DataValueField="ID"
                    DataTextField="NAME" class="list-group-item table table-bordered table-hover"></asp:ListBox>
            </div>
        </div>

        <div class="col-xs-6 col-md-6 col-sm-6 form-group" id="divEditPanel" runat="server" visible="false">
            <div class="row">
                <div class="col-sm-5 col-md-5 form-group">Category Name</div>
                <div class="col-sm-7 col-md-7 form-group">
                    <asp:TextBox ID="txtCategoryName" runat="server" CssClass="form-control"/>
                </div>
            </div>
            <div class="row" id="divFUpload" runat="server" visible="false">
                <div class="col-sm-5 col-md-5 form-group">Select Image</div>
                <div class="col-sm-7 col-md-7 form-group">
                    <asp:FileUpload ID="fuImage" runat="server" CssClass="form-control" data-toggle="tooltip" data-placement="bottom" title="select only '.png' format" />
                </div>
                <asp:HiddenField ID="hdGuid" runat="server" />               
                <div class="col-sm-5 col-md-5 form-group"></div>
                <div class="col-sm-7 col-md-7 form-group">
                    <asp:CheckBox ID="chkActive" runat="server" CssClass="checkbox" Checked="true" Text="Is Active" />
                </div>
            </div>
            <div class="row" id="divImage" runat="server">
                <div class="col-sm-5 col-md-5 form-group">Image</div>
                <div class="col-sm-7 col-md-7 form-group">
                    <asp:Image ID="imgCtg" runat="server" Height="200px" Width="200px" />
                </div> 
            </div>
            

            <div class="col-sm-5 col-md-5 form-group"></div>
            <div class="col-sm-7 col-md-7 form-group">
                <asp:Button ID="btnEditUpdate" runat="server" Text="Edit" CssClass="btn btn-success" OnClick="btnEditUpdate_Click" />
            </div>
        </div>
    </div>
</asp:Content>

