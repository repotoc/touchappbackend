﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyfashionsDB.Models;
using MyfashionsDB.Constants;
using System.IO;
using System.Net;

public partial class Admin_AddOffers : System.Web.UI.Page
{
    DropDownList ddlMain,ddlCateg;
    object lockTarget = new object();
    string sGuid;
    string frameURI;
    HttpClient client;
    public string companyadminid;
    public static Stores store;
    public static long strID;
    MasterOffers master = new MasterOffers();
    List<Stores> lstNewStores = new List<Stores>();
    public static List<Int64> lstVal = new List<Int64>();
    protected void Page_Load(object sender, EventArgs e)
    {

        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;
        companyadminid = Session["companyadminid"].ToString();
        if (!IsPostBack)
        {
            sGuid = Guid.NewGuid().ToString();
            hdGuid.Value = sGuid;
            string UriStore = "api/StoreLogin/GetAllStores?relatedcompanyid=" + companyadminid + "";
            HttpResponseMessage responseStore = client.GetAsync(UriStore).Result;
            if (responseStore.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                List<Stores> selectedStores = responseStore.Content.ReadAsAsync<List<Stores>>().Result;
                chkStores.DataSource = selectedStores.Where(c => c.ISACTIVE).ToList();
                chkStores.DataTextField = "Name";
                chkStores.DataValueField = "ID";
                chkStores.DataBind();
            }
            else
            {
                string errorMsg = responseStore.Content.ReadAsStringAsync().Result;
            }
        }
        ddlMain = new DropDownList();
        ddlMain.ID = "ddlMain";
        ddlMain.AutoPostBack = true;
        ddlMain.DataTextField = "NAME";
        ddlMain.DataValueField = "ID";
        ddlMain.CssClass = "slctdvalue form-control";
        try
        {
            long strusername = Convert.ToInt64(Session["companyadminid"].ToString());
            string uriOffer = "api/MasterOffers/GetAllOffersUnderCompany?relatedcompanyid=" + strusername;
            HttpResponseMessage responseOffer = client.GetAsync(uriOffer).Result;
            if (responseOffer.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                List<MasterOffers> main = responseOffer.Content.ReadAsAsync<List<MasterOffers>>().Result;
                ddlMain.DataSource = main;
                ddlMain.DataBind();
                ddlMain.Items.Insert(0, "Select");
                //ddlMain.SelectedIndexChanged += new EventHandler(Dynamic_Method);
                PlaceHolder1.Controls.Add(ddlMain);
            }
            else
            {
                string errorMsg = responseOffer.Content.ReadAsStringAsync().Result;
            }
            //MainEnum main = App.myFashionsDBContext.MainEnum.ToList().Where(c => c.NAME.Equals("Offers")).FirstOrDefault();
            //ddlMain.DataSource = main.LSTOFFERS;
            //ddlMain.DataBind();
            //ddlMain.Items.Insert(0, "Select");
            ////ddlMain.SelectedIndexChanged += new EventHandler(Dynamic_Method);
            //PlaceHolder1.Controls.Add(ddlMain);
            #region ddlMainBinding
            ddlCateg = new DropDownList();
            ddlCateg.ID = "ddlCateg";
            ddlCateg.AutoPostBack = true;
            ddlCateg.DataTextField = "NAME";
            ddlCateg.DataValueField = "SNO";
            ddlCateg.CssClass = "slctdvalue3 form-control";
            try
            {
                string uri = "api/MasterCategories/GetAllCategories?relatedcompanyid=" + companyadminid + "&fetchTopCategories=true";
                HttpResponseMessage response = client.GetAsync(uri).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    MainEnum mainC = response.Content.ReadAsAsync<MainEnum>().Result;
                    ddlCateg.DataSource = mainC.LSTCATEGORIES;
                    ddlCateg.DataBind();
                    ddlCateg.Items.Insert(0, "Select");
                    ddlCateg.SelectedIndexChanged += new EventHandler(Dynamic_Method);
                    PlaceHolder3.Controls.Add(ddlCateg);
                }
                else
                {
                    string errorMsg = response.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(GetType(), "notifierErr", "errorAlert('" + ex.Message + "');", true);
            }
            #endregion
        }
        catch (Exception ex)
        {
        }
    }

    private void Dynamic_Method(object sender, EventArgs e)
    {
        if (ddlCateg.SelectedItem.Text == "Select")
        {
            ddlCateg.Items.Clear();
            ddlCateg.DataSource = null;
            ddlCateg.DataBind();
        }
    }
    protected void hdntxtvalue2_TextChanged(object sender, EventArgs e)
    {
        TextBox selectedText = ((TextBox)sender);
        if (selectedText != null)
        {
            if (selectedText.Text != "Select")
            {
                long selectCategId = Convert.ToInt64(selectedText.Text);
                 lstVal.Add(selectCategId);
                string uri = "api/MasterCategories/GetSubCategories?relatedmastercategoryid=" + selectCategId + "";
                HttpResponseMessage response = client.GetAsync(uri).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    List<MasterCategories> master = response.Content.ReadAsAsync<List<MasterCategories>>().Result;
                    int cnt = FindOccurence("ddl");
                    long selectedddlcnt = cnt + 1;
                    if (master.Count > 0)
                        BuildDynamicDropDown(master, selectedddlcnt);
                }
                else
                {
                    string errorMsg = response.Content.ReadAsStringAsync().Result;
                }
            }
        }
    }

    private void BuildDynamicDropDown(List<MasterCategories> master, long selectedddlcnt)
    {
        DropDownList ddl1 = new DropDownList();
        ddl1.ID = "ddl" + selectedddlcnt;
        ddl1.DataSource = master;
        ddl1.DataTextField = "NAME";
        ddl1.DataValueField = "SNO";
        ddl1.CssClass = "slctdvalue4 form-control mg-top";
        ddl1.EnableViewState = true;
        ddl1.DataBind();
        ddl1.Items.Insert(0, new ListItem("Select", "Select", true));
        PlaceHolder4.Controls.Add(ddl1);
    }
    protected void hdntxtvalue_TextChanged(object sender, EventArgs e)
    {
        TextBox selectedText = ((TextBox)sender);
        if (selectedText != null)
        {
            if (selectedText.Text != "Select")
            {
                long selectOfferId = Convert.ToInt64(selectedText.Text);
                strID = selectOfferId;
                string uri = "api/MasterOffers/GetOfferByMasterOfferID?relatedMasterOfferID=" + selectOfferId + "";
                HttpResponseMessage response = client.GetAsync(uri).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    master = response.Content.ReadAsAsync<MasterOffers>().Result;
                    int cnt = FindOccurence("ddl");
                    long selectedddlcnt = cnt + 1;
                    if (master.LSTSUBOFFERS.Count > 0)
                        BuildDynamicDropDown(master.LSTSUBOFFERS, selectedddlcnt);
                }
                else
                {
                    string errorMsg = response.Content.ReadAsStringAsync().Result;
                }
            }
        }
    }
    private int FindOccurence(string substr)
    {
        string reqstr = Request.Form.ToString();
        return ((reqstr.Length - reqstr.Replace(substr, "").Length) / substr.Length);
    }
    public static int i = 0;
    private void BuildDynamicDropDown(IList<MasterOffers> list, long uniqueID)
    {
        DropDownList ddl = new DropDownList();
        ddl.ID = "ddl" + uniqueID;
        ddl.DataSource = list;
        ddl.DataTextField = "NAME";
        ddl.DataValueField = "SNO";
        ddl.CssClass = "slctdvalue1 form-control mg-top";
        ddl.EnableViewState = true;
        ddl.DataBind();
        ddl.Items.Insert(0, new ListItem("Select", "Select", true));
        PlaceHolder2.Controls.Add(ddl);
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        long val = strID;
        try
        {
            MasterOffers masterOffer = new MasterOffers();
            masterOffer.NAME = txtOfferName.Text.ToUpper();
            masterOffer.DESCRIPTION = txtDescription.Text.Trim();
            lstVal.ForEach(c =>
            {
                string uriSingleCateg = "api/MasterCategories/GetSingleCategory?relatedcategoryid=" + c + "";
                HttpResponseMessage response = client.GetAsync(uriSingleCateg).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    MasterCategories selectedMainCategory = response.Content.ReadAsAsync<MasterCategories>().Result;
                    if (masterOffer.LSTAPPLIEDSUBCATEGORIES != null)
                    {
                        masterOffer.LSTAPPLIEDSUBCATEGORIES.Add(selectedMainCategory);
                    }
                    else
                    {
                        masterOffer.LSTAPPLIEDSUBCATEGORIES = new List<MasterCategories>();
                        masterOffer.LSTAPPLIEDSUBCATEGORIES.Add(selectedMainCategory);
                    }
                }
                else
                {
                    string errorMsg = response.Content.ReadAsStringAsync().Result;
                }
            });
                string uri = "api/MasterOffers/GetOfferByMasterOfferID?relatedMasterOfferID=" + strID + "";
                HttpResponseMessage responseMaster = client.GetAsync(uri).Result;
                if (responseMaster.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    master = responseMaster.Content.ReadAsAsync<MasterOffers>().Result;
                    if (master.LSTSUBOFFERS != null)
                    {
                        master.LSTSUBOFFERS.Add(masterOffer);
                    }
                    else
                    {
                        master.LSTSUBOFFERS = new List<MasterOffers>();
                        master.LSTSUBOFFERS.Add(masterOffer);
                    }
                }
            if (fuOfferImage.HasFile)
            {
                string fileName = Path.GetFileName(fuOfferImage.PostedFile.FileName);
                string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtOfferName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                string path = @"D:\Client Management Component\Users\TouchBackEndImages1";
                string folder = path + @"\OfferImages";
                string ImagesFolder = "OfferImages";
                string ServerPath = Server.MapPath("~/OfferImages");
                if (!Directory.Exists(ServerPath))
                {
                    Directory.CreateDirectory(ServerPath);
                }
                fuOfferImage.PostedFile.SaveAs(Path.Combine(ServerPath, hdGuid.Value + "~" + strRandomNumber));

                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                fuOfferImage.PostedFile.SaveAs(Path.Combine(folder, hdGuid.Value + "~" + strRandomNumber));
                masterOffer.IMAGENAME = hdGuid.Value + "~" + strRandomNumber;
                masterOffer.IMAGELOCATION = "pack://siteoforigin:,,,/" + ImagesFolder + "/" + hdGuid.Value + "~" + strRandomNumber;
                masterOffer.WEBIMAGELOCATION = folder + hdGuid.Value + "~" + strRandomNumber;
            }
            if (ddlDiscountType.SelectedValue.Equals("B"))
            {
                masterOffer.BUYVALUE = Convert.ToInt32(txtBuyValue.Text);
                masterOffer.GETVALUE = Convert.ToInt32(txtGetValue.Text);
                DiscountTypes.BuyGetOffer.Equals("B");
            }
            else if (ddlDiscountType.SelectedValue.Equals("P"))
            {
                masterOffer.DISCOUNT = Convert.ToDecimal(txtOfferValue.Text);
                DiscountTypes.PercentageDiscount.Equals("P");
            }
            else if (ddlDiscountType.SelectedValue.Equals("R"))
            {
                masterOffer.DISCOUNT = Convert.ToDecimal(txtOfferValue.Text);
                masterOffer.DISCOUNTTYPE = "R";
                DiscountTypes.PercentageDiscount.Equals("R");
            }
            else if (ddlDiscountType.SelectedValue.Equals("C"))
            {
                masterOffer.DISCOUNT = Convert.ToDecimal(txtOfferValue.Text);
                DiscountTypes.PercentageDiscount.Equals("C");
            }
            else if (ddlDiscountType.SelectedValue.Equals("N"))
            {
                masterOffer.DISCOUNTTYPE = "N";
            }
            masterOffer.LASTUPDATEDTIME = DateTime.Now;
            masterOffer.VERSION = 1;
            string uriCompany = "api/CompanyLogin/GetCompanyInfo?companyID=" + companyadminid + "";
            HttpResponseMessage responseCmp = client.GetAsync(uriCompany).Result;
            if (responseCmp.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                Company selectedCompany = responseCmp.Content.ReadAsAsync<Company>().Result;
                masterOffer.REGCOMPANY = selectedCompany;
            }
            else
            {
                string errorMsg = responseCmp.Content.ReadAsStringAsync().Result;
            }
            if (chkexpired.Checked)
            {
                masterOffer.ISEXPIRED = true;
            }
            else
            {
                masterOffer.ISEXPIRED = false;
            }
            List<string> lstStores = new List<string>();
            foreach (ListItem item in chkStores.Items)
            {
                if (item.Selected)
                {
                    long id = Convert.ToInt64(item.Value);
                    string uriStore = "api/StoreLogin/GetStoreInfo?relatedstoreid=" + id;
                    HttpResponseMessage responseStore = client.GetAsync(uriStore).Result;
                    if (responseStore.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                    {
                        store = new Stores();
                        store = responseStore.Content.ReadAsAsync<Stores>().Result;
                    }
                    else
                    {
                        string errorMsg = responseStore.Content.ReadAsStringAsync().Result;
                    }
                    lstNewStores.Add(store);
                }
            }

            masterOffer.LSTREGSTORE = lstNewStores;
            string uriCreateOffer = "api/MasterOffers/MasterOffersCRUDOperation?parameter=C";
            HttpResponseMessage responseCreateCmp = client.PostAsJsonAsync<MasterOffers>(uriCreateOffer, masterOffer).Result;
            if (responseCreateCmp.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                string strnew = Newtonsoft.Json.JsonConvert.SerializeObject(masterOffer);
                ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Sub Offer Created Successfully');", true);
            }
            else
            {
                string strnew = Newtonsoft.Json.JsonConvert.SerializeObject(masterOffer);
                string errorMsg = responseCreateCmp.Content.ReadAsStringAsync().Result;
            }

        }
        catch (Exception ex)
        {

        }

    }
    protected void ddlDiscountType_SelectedIndexChanged(object sender, EventArgs e)
    {
        divBuyGet.Visible = false;
        divRPC.Visible = false;
        lblType.Text = "";
        if (ddlDiscountType.SelectedIndex > 0)
        {
            if (ddlDiscountType.SelectedValue.Equals("B"))
            {
                divBuyGet.Visible = true;
            }
            else if (ddlDiscountType.SelectedValue.Equals("R"))
            {
                divRPC.Visible = true;
                lblType.Text = "Rs";
            }
            else if (ddlDiscountType.SelectedValue.Equals("P"))
            {
                divRPC.Visible = true;
                lblType.Text = "%";
            }
            else if (ddlDiscountType.SelectedValue.Equals("C"))
            {
                divRPC.Visible = true;
            }
          
        }
    }
    protected void btnSelectedCateg_Click(object sender, EventArgs e)
    {
        long SNO = Convert.ToInt64(hdntxtvalue2.Text);
        if (!string.IsNullOrEmpty(hdntxtvalue2.Text))
        {
             string uriSingleCateg = "api/MasterCategories/GetSingleCategory?relatedcategoryid=" + SNO + "";
                HttpResponseMessage response = client.GetAsync(uriSingleCateg).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    MasterCategories mCateg = response.Content.ReadAsAsync<MasterCategories>().Result;
                    if (mCateg.LSTSUBCATEGORIES.Count > 0 && mCateg.LSTSUBCATEGORIES != null)
                    {
                        chkCateg.DataSource = mCateg.LSTSUBCATEGORIES;
                        chkCateg.DataBind();
                    }

                }
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
}