﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="AddOffers.aspx.cs" Inherits="Admin_AddOffers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {

            //CHECKING OF LOCALSTORAGE AND PREPENDING OF SELECT BOXES
            if (localStorage.getItem("htmldata0") != " " && localStorage.getItem("htmldata0") !== "undefined") {
                for (i = 0; i <= parseInt(dc) ; i++) {
                    var htmldata = localStorage.getItem("htmldata" + i + "");
                    $(".dsata").append("<select class='slctdvalue1 form-control mg-top' name=" + i + ">" + htmldata + "</select>");
                }
                //SETTING OF VALUES FOR DROPDOWN BY USING LOACLSTORAGE
                for (i = 0; i <= parseInt(dc) ; i++) {
                    var a = localStorage.getItem("dfltdata" + i + "");
                    $(".slctdvalue1 option").each(function (e, i) {
                        var b = $(this).val();
                        if (a == b) {
                            $(this).parent().val(a);
                        }
                    });
                }
            }

            //REST OF TEXTBOX CHANGE FUNCTION AND CHANGE FUNCTION
            //$(document).on("change", ".slctdvalue", function () {
            //    $(".dsata").html("");
            //});

            //REST OF TEXTBOX CHANGE FUNCTION AND CHANGE FUNCTION
            $(document).on("change", ".slctdvalue,.slctdvalue1", function () {
                $(".hdnvalue").val($(this).val());
                $(".hdnvalue").change();
                $(".slctdvalue1").each(function (e, i) {
                    localStorage.setItem("htmldata" + e + "", $(this).html());
                    localStorage.setItem("dfltdata" + e + "", $(this).val());
                    localStorage.setItem("noof", e);
                });
            });


            //CHECKING OF LOCALSTORAGE AND PREPENDING OF SELECT BOXES          
            if (localStorage.getItem("htmldata20") != " " && localStorage.getItem("htmldata20") !== "undefined") {
                for (i = 0; i <= parseInt(dc2) ; i++) {
                    var htmldata2 = localStorage.getItem("htmldata2" + i + "");
                    $(".dsata2").append("<select class='slctdvalue4 form-control mg-top' name=" + i + ">" + htmldata2 + "</select>");
                }
                //SETTING OF VALUES FOR DROPDOWN BY USING LOACLSTORAGE
                for (i = 0; i <= parseInt(dc2) ; i++) {
                    var a = localStorage.getItem("dfltdata2" + i + "");
                    $(".slctdvalue4 option").each(function (e, i) {
                        var b = $(this).val();
                        if (a == b) {
                            $(this).parent().val(a);
                        }
                    });
                }
            }

            //REST OF TEXTBOX CHANGE FUNCTION AND CHANGE FUNCTION
            $(document).on("change", ".slctdvalue3,.slctdvalue4", function () {
                $(".hdnvalue2").val($(this).val());
                $(".hdnvalue2").change();
                $(".slctdvalue4").each(function (e, i) {
                    localStorage.setItem("htmldata2" + e + "", $(this).html());
                    localStorage.setItem("dfltdata2" + e + "", $(this).val());
                    localStorage.setItem("noof2", e);
                });
            })
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ol class="breadcrumb">
        <li><a href="AdminHome.aspx">Home</a></li>
        <li>Offers</li>
        <li class="active">Add Offer</li>
    </ol>

    <div class="row form-group">
        <div class="col-sm-12 col-md-5 form-group">
             <asp:PlaceHolder ID="PlaceHolder1" runat="server" />
            <div class="dsata" id="divCateg" runat="server"></div>
            <asp:PlaceHolder ID="PlaceHolder2" runat="server"/>
            <asp:TextBox ID="hdntxtvalue" runat="server" OnTextChanged="hdntxtvalue_TextChanged" AutoPostBack="true" CssClass="hdnvalue" Style="display: none;" />
            <asp:TextBox ID="hdnvaluedync" runat="server" CssClass="hdnvalue" Style="display: none;" />
        </div>
        <div class="col-sm-12 col-md-7 form-group">
            <div class="row form-group">
                <div class="col-sm-5 col-md-5">Offer Name</div>
                <div class="col-sm-7 col-md-7">
                    <asp:TextBox ID="txtOfferName" runat="server" CssClass="form-control" placeholder="Offer Name" />
                </div>
            </div>
        <div class="row form-group">
                <div class="col-sm-5 col-md-5">Categories</div>
                 <div class="col-sm-7 col-md-7">
                    <div class="form-group">
                            <asp:PlaceHolder ID="PlaceHolder3" runat="server" />
            <div class="dsata2"></div>
            <asp:PlaceHolder ID="PlaceHolder4" runat="server" />
            <asp:TextBox ID="hdntxtvalue2" runat="server" OnTextChanged="hdntxtvalue2_TextChanged" AutoPostBack="true" CssClass="hdnvalue2" Style="display: none;" />
            <asp:TextBox ID="hdnvaluedync1" runat="server" CssClass="hdnvalue3" Style="display: none;" />
                            <asp:Button ID="btnSelectedCateg" runat="server" CssClass="btn btn-info btn-md btn-block mg-top" Text="Select Category Linking" OnClick="btnSelectedCateg_Click" />
                    </div>
                    <div class="form-group">
                            <div class="panel panel-default">
                                <!-- Default panel contents -->
                                <div class="panel-heading">Selected Category List</div>
                                <div class="panel-body" style="padding-left: 35px;">
                                    <asp:CheckBoxList ID="chkCateg" runat="server"  CssClass="checkbox-1" Style="font-size: 12pt;" AppendDataBoundItems="true" AutoPostBack="true"  DataTextField="NAME" DataValueField="SNO"  SelectionMode="Multiple"></asp:CheckBoxList>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-5 col-md-5">Stores</div>
                <div class="col-sm-7 col-md-7">
                    <asp:CheckBoxList ID="chkStores" runat="server" CssClass="checkbox-1" Style="font-size: 12pt;" AppendDataBoundItems="true" AutoPostBack="true" SelectionMode="Multiple"></asp:CheckBoxList>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-5 col-md-5">Description</div>
                <div class="col-sm-7 col-md-7">
                    <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control multiline" placeholder="Description" TextMode="MultiLine" />
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-5 col-md-5">Discount Type</div>
                <div class="col-sm-7 col-md-7">
                    <asp:DropDownList ID="ddlDiscountType" runat="server" AppendDataBoundItems="true" CssClass="form-control"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlDiscountType_SelectedIndexChanged">
                        <asp:ListItem Value="-1" Text="Select" Selected="True" />
                        <asp:ListItem Value="B" Text="Buy-Get" />
                        <asp:ListItem Value="R" Text="Rupee" />
                        <asp:ListItem Value="P" Text="Percentage" />
                        <asp:ListItem Value="C" Text="Complementary Gift" />
                        <asp:ListItem Value="N" Text="None" />
                    </asp:DropDownList>
                </div>
            </div>
            <div class="row form-group">
               <div class="col-sm-5 col-md-5"></div>
                <div class="col-sm-7 col-md-7">
                   <asp:CheckBox ID="chkexpired" runat="server" Text="IsActive"/>  
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-5 col-md-5">Select Image</div>
                <div class="col-sm-7 col-md-7">
                    <asp:FileUpload ID="fuOfferImage" runat="server" CssClass="form-control" data-toggle="tooltip" data-placement="bottom" title="select only '.png' format" />
                    <asp:HiddenField ID="hdGuid" runat="server" />
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                </div>
                <div class="col-md-7 col-sm-12">
                    <div class="row col-md-12 col-sm-12" id="divBuyGet" runat="server" visible="false">
                        <div class="row col-md-6 col-sm-6">
                            <asp:TextBox ID="txtBuyValue" runat="server" CssClass="form-control" placeholder="Buy Value" /></div>
                        <div class="col-md-6 col-sm-6">
                            <asp:TextBox ID="txtGetValue" runat="server" CssClass="form-control" placeholder="Get Value" /></div>
                    </div>
                    <div class="form-group row col-md-12 col-sm-12" id="divRPC" runat="server" visible="false">
                        <div class="row col-md-11 col-sm-11">
                            <asp:TextBox ID="txtOfferValue" runat="server" CssClass="form-control" /></div>
                        <div class="col-md-1 col-sm-1 lblSideHeading" style="margin-left: 10px;">
                            <asp:Label ID="lblType" runat="server" /></div>
                    </div>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-5 col-md-5 form-group"></div>
                <div class="col-sm-7 col-md-7 form-group">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn btn-success" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click"  />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

