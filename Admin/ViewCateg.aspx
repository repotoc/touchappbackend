﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ViewCateg.aspx.cs" Inherits="Admin_ViewCateg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        $(document).ready(function () {

            //CHECKING OF LOCALSTORAGE AND PREPENDING OF SELECT BOXES
            var dc = localStorage.getItem("noof");
            if (localStorage.getItem("htmldata0") != " " && localStorage.getItem("htmldata0") !== "undefined") {
                for (i = 0; i <= parseInt(dc) ; i++) {
                    var htmldata = localStorage.getItem("htmldata" + i + "");

                    $(".dsata").append("<select class='slctdvalue1 form-control mg-top' name=" + i + ">" + htmldata + "</select>");
                }
                //SETTING OF VALUES FOR DROPDOWN BY USING LOACLSTORAGE
                for (i = 0; i <= parseInt(dc) ; i++) {
                    //alert(localStorage.getItem("dfltdata1"));

                    var a = localStorage.getItem("dfltdata" + i + "");
                    $(".slctdvalue1 option").each(function (e, i) {
                        var b = $(this).val();

                        if (a == b) {
                            $(this).parent().val(a);
                        }
                    });
                }
            }
            //CLEARING OF LOCAL STORAGE
            for (i = 0; i <= parseInt(dc) ; i++) {

                localStorage.setItem("htmldata" + i + "", " ");
            }
            //FIRST TEXTBOX CHANGE FUNCTION
            $(".slctdvalue").change(function () {
                $(".hdnvalue").val($(this).val());
                $(".hdnvalue").change();
            })

            //REST OF TEXTBOX CHANGE FUNCTION AND CHANGE FUNCTION
            $(document).on("change", ".slctdvalue1", function () {

                $(".hdnvalue").val($(this).val());
                $(".hdnvalue").change();
                $(".slctdvalue1").each(function (e, i) {

                    localStorage.setItem("htmldata" + e + "", $(this).html());
                    localStorage.setItem("dfltdata" + e + "", $(this).val());

                    localStorage.setItem("noof", e);
                });
            })

        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <ol class="breadcrumb">
        <li><a href="AdminHome.aspx">Home</a></li>
        <li>Categories</li>
        <li class="active">View Category</li>
    </ol>

    <div class="col-xs-12 col-md-12 col-sm-12">

        <div class="col-xs-6 col-md-6 col-sm-6 form-group">
            <div class="row form-group">
                <asp:PlaceHolder ID="PlaceHolder1" runat="server" />
                <div class="dsata"></div>
                <asp:PlaceHolder ID="PlaceHolder2" runat="server" />
                <asp:TextBox ID="hdntxtvalue" runat="server" OnTextChanged="hdntxtvalue_TextChanged" AutoPostBack="true" CssClass="hdnvalue" Style="display: none;" />
                <asp:TextBox ID="hdnvaluedync" runat="server" CssClass="hdnvalue1" Style="display: none;" />
            </div>
        </div>

        <div class="col-xs-6 col-md-6 col-sm-6 form-group" id="divEditPanel" runat="server" visible="false">
            <div class="row">
                <div class="col-sm-5 col-md-5 form-group">Category Name</div>
                <div class="col-sm-7 col-md-7 form-group">
                    <asp:TextBox ID="txtCategoryName" runat="server" CssClass="form-control"/>
                </div>
            </div>
            <div class="row" id="divFUpload" runat="server" visible="false">
                <div class="col-sm-5 col-md-5 form-group">Select Image</div>
                <div class="col-sm-7 col-md-7 form-group">
                    <asp:FileUpload ID="fuImage" runat="server" CssClass="form-control"  data-toggle="tooltip" data-placement="bottom" title="select only '.png' format" />
                </div>
                <asp:HiddenField ID="hdGuid" runat="server"/>               
                <div class="col-sm-5 col-md-5 form-group"></div>
                <div class="col-sm-7 col-md-7 form-group">
                    <asp:CheckBox ID="chkActive" runat="server" CssClass="checkbox" Checked="true" Text="Is Active" />
                </div>
            </div>
            <div class="row" id="divImage" runat="server">
                <div class="col-sm-5 col-md-5 form-group">Image</div>
                <div class="col-sm-7 col-md-7 form-group">
                    <asp:Image ID="imgCtg" runat="server" Height="200px" Width="200px" />
                </div>
            </div>
            <div class="col-sm-5 col-md-5 form-group"></div>
            <div class="col-sm-7 col-md-7 form-group">
                <asp:Button ID="btnEditUpdate" runat="server" Text="Edit" CssClass="btn btn-success" OnClick="btnEditUpdate_Click" />
            </div>
        </div>
    </div>
</asp:Content>

