﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMasterPage.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script type="text/javascript">
         $(document).ready(function () {
             $(".PwdTxt").change(function () {
                 var NewPwd = $('.newpwd').val();
                 if (NewPwd.length < 6) {
                     $('.msg1').text("Atleast 6 Characters required");
                     $(".savebtn").attr("disabled", "disabled");
                 }
                 else
                     $('.msg1').text("");
             })

             $(".PwdTxt").keyup(function () {
                 var NewPwd = $('.newpwd').val();
                 $('.cnfpwd').val("");
                 $('.msg').text("");
                 if (NewPwd.length > 5)
                     $('.msg1').text("");
             })

             $(".chkPwd").keyup(function () {
                 var NewPwd = $('.newpwd').val();
                 var Cnfpwd = $('.cnfpwd').val();
                 if (NewPwd == Cnfpwd && NewPwd.length > 5 && Cnfpwd.length > 5) {
                     $(".msg").text(" Password Match ! ");
                     $(".savebtn").removeAttr("disabled");
                 } else {
                     $(".msg").text(" Password Do Not Match ! ");
                     $(".savebtn").attr("disabled", "disabled");

                 }
             })
         });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <ol class="breadcrumb">
        <li><a href="UserHome.aspx">Home</a></li>
        <li>Settings</li>
        <li class="active">Change Password</li>
    </ol>

        <div class="row form-group">
            <div class="col-md-4 col-sm-12">
                <div class="lblSideHeading">Old Password</div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="input-group">
                    <asp:TextBox ID="txtoldpwd" runat="server" class="form-control" TabIndex="1" placeholder="Old Password" 
                        aria-describedby="basic-addon1" data-toggle="tooltip" data-placement="bottom" TextMode="Password" title="Enter old password"/>
                    <span class="input-group-addon" id="basic-addon1">
                        <asp:CheckBox ID="chkOldPwd" runat="server" CssClass="checkbox-1" Text="show" TextAlign="Right"/></span>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4 col-sm-12">
                <div class="lblSideHeading">New Password</div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="input-group">
                    <asp:TextBox ID="txtnewpwd" runat="server" placeholder="New Password" TabIndex="2" class="form-control newpwd PwdTxt" 
                        aria-describedby="basic-addon2" data-toggle="tooltip" data-placement="bottom" TextMode="Password" title="Enter new password"/>
                    <span class="input-group-addon" id="basic-addon2">
                        <asp:CheckBox ID="chkNewPwd" runat="server" CssClass="checkbox-1" Text="show" TextAlign="Right"/></span>
                </div>
                <div id="Div1" class="msg-code-lg msg1"></div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4 col-sm-12">
                <div class="lblSideHeading">Confirm New Password</div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="input-group">
                    <asp:TextBox ID="txtcnfpwd" runat="server" placeholder="Confrim Password" TabIndex="3" class="form-control cnfpwd chkPwd"
                         aria-describedby="basic-addon3" data-toggle="tooltip" data-placement="bottom" TextMode="Password" title="Confirm password"/>
                    <span class="input-group-addon" id="basic-addon3">
                        <asp:CheckBox ID="chkCnfPwd" runat="server" CssClass="checkbox-1" Text="show" TextAlign="Right"/></span>
                </div>
                <div id="confirmMessage" class="msg-code-lg msg"></div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4 col-sm-12">
            </div>
            <div class="col-md-6 col-sm-12" style="margin-left: 15px;">
                <div class="row form-group">
                    <asp:Button ID="btnSubmit" class="btn btn-success savebtn" runat="server" TabIndex="4" disabled="disabled" Text="Save" OnClick="btnSubmit_Click"/>
                    <asp:Button ID="btnClear" runat="server" Text="Clear" class="btn btn-default" TabIndex="5" OnClick="btnClear_Click" />
                </div>
            </div>
        </div>
    
        <div class="row form-group alert alert-success" role="alert" id="divSuccessRedirectionMsg" runat="server" visible="false">
            <strong>Successfully Changed Password.</strong> Please Login again.
        </div>
   
</asp:Content>

