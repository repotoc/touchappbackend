﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyfashionsDB.Models;
using System.Net.Http;
using System.Net.Http.Headers;

public partial class _Default : System.Web.UI.Page
{
    string frameURI;
    HttpClient client; 
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;

        string uri = "api/SuperAdmin/InitiateSeed?initiateseed=true";
        HttpResponseMessage response = client.GetAsync(uri).Result;
        if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
        {
            //bool runSeed = new MyfashionsDB.InitiateSeed.RunDatabaseSeed().RunSeed(App.myFashionsDBContext);
        }
        else
        {
            string errorMsg = response.Content.ReadAsStringAsync().Result;
        }
    }
    protected void btnSuperAdmin_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/SuperAdmin/Default.aspx");
    }
    protected void btnCompanyAdmin_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/Default.aspx");
    }
    protected void btnStoreAdmin_Click(object sender, EventArgs e)
    {
        Response.Redirect("storeLoginPage.aspx");
    }
}