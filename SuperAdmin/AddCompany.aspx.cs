﻿using MyfashionsDB.Constants;
using MyfashionsDB.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SuperAdmin_AddCompany : System.Web.UI.Page
{
    object lockTarget = new object();
    string sGuid;
    string frameURI;
    HttpClient client;
    string errorMsg;
    public static Company company;

    protected void Page_Load(object sender, EventArgs e)
    {

        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;
        if (!IsPostBack)
        {
            if (Request.QueryString["ID"] != null)
            {
                LoadCompanyEditData();
            }
            else
            {
                App.Logger.Info("Starts PageLoad in Add Company Page");
                sGuid = Guid.NewGuid().ToString();
                hdGuid.Value = sGuid;

                DiscountTypes.BuyGetOffer.Equals("B");
            }
        }
    }

    private void LoadCompanyEditData()
    {

        long ID = Convert.ToInt64(Request.QueryString["ID"]);
        string uri = "api/CompanyLogin/GetCompanyInfo?companyID=" + ID + "";
        HttpResponseMessage response = client.GetAsync(uri).Result;

        if (response.StatusCode.Equals(HttpStatusCode.OK))
        {
            company = response.Content.ReadAsAsync<Company>().Result;
            if (company != null)
            {
                btnSave.Text = "Update";
                btnCancel.Text = "Reset";
                txtCompanyId.Text = company.COMPANYID;
                txtcompanyName.Text = company.NAME;
                txtusername.Text = company.USERNAME;
                txtusername.Attributes.Add("readonly", "true");
                txtPassword.Text = company.PASSWORD;
                txtCity.Text = company.CITY;
                txtState.Text = company.STATE;
                txtadd1.Text = company.ADDRES1;
                txtadd2.Text = company.ADDRESS2;
                txtnoemp.Text = company.NUMEMPLOYEES.ToString();
                if (company.ISACTIVE)
                    chkActive.Checked = true;
                else
                    chkActive.Checked = false;
            }
        }
        else
        {
            errorMsg = response.Content.ReadAsStringAsync().Result;
            ClientScript.RegisterStartupScript(GetType(), "notifier10", "errorAlert('" + errorMsg + "');", true);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (btnSave.Text == "Save")
            {
                company = new Company();
                company.COMPANYID = txtCompanyId.Text;
                company.NAME = txtcompanyName.Text.Trim().ToUpper();
                company.USERNAME = txtusername.Text.Trim().ToUpper();
                company.PASSWORD = txtPassword.Text.Trim();
                company.CITY = txtCity.Text.Trim().ToUpper();
                company.STATE = txtState.Text.Trim().ToUpper();
                company.ADDRES1 = txtadd1.Text.Trim().ToUpper();
                company.ADDRESS2 = txtadd2.Text.Trim().ToUpper();
                company.NUMEMPLOYEES = Convert.ToInt32(txtnoemp.Text);
                if (fplogo.HasFile)
                {
                    string fileName = Path.GetFileName(fplogo.PostedFile.FileName);
                    string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtcompanyName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                    string path = @"D:\Client Management Component\Users\TouchBackEndImages";
                    string folder = path + @"\Logo";
                    string ImagesFolder = "Logo";

                    string ServerPath = Server.MapPath("~/SuperAdmin/Logo");
                    if (!Directory.Exists(ServerPath))
                    {
                        Directory.CreateDirectory(ServerPath);
                    }
                    fplogo.PostedFile.SaveAs(Path.Combine(ServerPath, hdGuid.Value + "~" + strRandomNumber));

                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    fplogo.PostedFile.SaveAs(Path.Combine(folder, hdGuid.Value + "~" + strRandomNumber));
                    company.IMAGENAME = hdGuid.Value + "~" + strRandomNumber;
                    company.IMAGELOCATION = "pack://siteoforigin:,,,/" + ImagesFolder + "/" + hdGuid.Value + "~" + strRandomNumber;
                    company.WEBIMAGELOCATION = folder + hdGuid.Value + "~" + strRandomNumber;
                }
                company.VERSION = 1;
                if (chkActive.Checked)
                {
                    company.ISACTIVE = true;
                }
                else
                {
                    company.ISACTIVE = false;
                }
                company.LASTUPDATEDTIME = DateTime.Now;
                long superadminId = Convert.ToInt64(Session["superadminId"]);

                #region GetSuperAdminByID
                string uriSuperAdmin = "api/SuperAdmin/GetSuperAdminByID?superAdminID=" + superadminId + "";
                HttpResponseMessage responseSA = client.GetAsync(uriSuperAdmin).Result;
                if (responseSA.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    SuperAdmin superAdm = responseSA.Content.ReadAsAsync<SuperAdmin>().Result;
                    company.REGISTEREDSUPERADMIN = superAdm;
                }
                else
                {
                    errorMsg = responseSA.Content.ReadAsStringAsync().Result;
                    ClientScript.RegisterStartupScript(GetType(), "notifier11", "errorAlert('" + errorMsg + "');", true);
                }
                #endregion

                #region CRUDOperationCreateCompany
                string uri = "api/CompanyLogin/CompanyCRUDOperation?parameter=C";
                HttpResponseMessage response = client.PostAsJsonAsync<Company>(uri, company).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('StoreAdmin Created Successfully');", true);
                }
                else
                {
                    //string str= Newtonsoft.Json.JsonConvert.SerializeObject(company);
                    errorMsg = response.Content.ReadAsStringAsync().Result;
                    ClientScript.RegisterStartupScript(GetType(), "notifier12", "errorAlert('" + errorMsg + "');", true);
                }
                #endregion
                ClearAll();
            }
            else if (btnSave.Text == "Update")
            {
                company.COMPANYID = txtCompanyId.Text;
                company.NAME = txtcompanyName.Text.Trim().ToUpper();
                company.USERNAME = txtusername.Text.Trim().ToUpper();
                company.PASSWORD = txtPassword.Text.Trim();
                company.CITY = txtCity.Text.Trim().ToUpper();
                company.STATE = txtState.Text.Trim().ToUpper();
                company.ADDRES1 = txtadd1.Text.Trim().ToUpper();
                company.ADDRESS2 = txtadd2.Text.Trim().ToUpper();
                company.NUMEMPLOYEES = Convert.ToInt32(txtnoemp.Text);
                if (fplogo.HasFile)
                {
                    string fileName = Path.GetFileName(fplogo.PostedFile.FileName);
                    string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtcompanyName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                    string path = @"D:\Client Management Component\Users\TouchBackEndImages";
                    string folder = path + @"\Logo";
                    string ImagesFolder = "Logo";

                    string ServerPath = Server.MapPath("~/SuperAdmin/Logo");
                    if (!Directory.Exists(ServerPath))
                    {
                        Directory.CreateDirectory(ServerPath);
                    }
                    fplogo.PostedFile.SaveAs(Path.Combine(ServerPath, hdGuid.Value + "~" + strRandomNumber));

                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    fplogo.PostedFile.SaveAs(Path.Combine(folder, hdGuid.Value + "~" + strRandomNumber));
                    company.IMAGENAME = hdGuid.Value + "~" + strRandomNumber;
                    company.IMAGELOCATION = "pack://siteoforigin:,,,/" + ImagesFolder + "/" + hdGuid.Value + "~" + strRandomNumber;
                    company.WEBIMAGELOCATION = folder + hdGuid.Value + "~" + strRandomNumber;
                }
                company.VERSION = company.VERSION + 1;
                if (chkActive.Checked)
                {
                    company.ISACTIVE = true;
                }
                else
                {
                    company.ISACTIVE = false;
                }
                company.LASTUPDATEDTIME = DateTime.Now;
                #region CRUDOperationUpdateCompany
                string uri = "api/CompanyLogin/CompanyCRUDOperation?parameter=U";
                HttpResponseMessage response = client.PostAsJsonAsync<Company>(uri, company).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Company Updated Successfully');setInterval(function(){window.location='ViewCompanies.aspx';},1000);", true);
                }
                else
                {
                    errorMsg = response.Content.ReadAsStringAsync().Result;
                    ClientScript.RegisterStartupScript(GetType(), "notifier14", "errorAlert('" + errorMsg + "');", true);
                }
                #endregion
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(GetType(), "notifierErr", "errorAlert('" + ex.Message + "');", true);
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (btnCancel.Text == "Cancel")
        {
            ClearAll();
        }
        else if (btnCancel.Text == "Reset")
        {
            LoadCompanyEditData();
        }
    }
    private void ClearAll()
    {
        txtcompanyName.Text = string.Empty;
        txtnoemp.Text = string.Empty;
        txtusername.Text = string.Empty;
        txtPassword.Text = string.Empty;
        txtCity.Text = string.Empty;
        txtState.Text = string.Empty;
        txtadd1.Text = string.Empty;
        txtadd2.Text = string.Empty;
        txtnoemp.Text = string.Empty;
        chkActive.Checked = false;
    }
}