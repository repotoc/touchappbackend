﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="SuperAdmin_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>touchOnCloud</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="../css/alertify.min.css" rel="stylesheet" />
    <link href="../css/themes/default.rtl.css" rel="stylesheet" />    
    <script src="../Js/alertify.min.js"></script>
    <link href="../css/font-awesome.min.css" rel="stylesheet"/>
    <script type="text/javascript">
        function successAlert(msg) {
            alertify.success(msg);
        }
        function warningAlert(msg) {
            alertify.warning(msg);
        }
        function errorAlert(msg) {
            alertify.error(msg);
        }
        function messageAlert(msg) {
            alertify.message(msg);
        }
    </script>
</head>
<body>
   <form id="form1" runat="server" role="form" autocomplete="off">
        <div class="col-xs-8 col-sm-8 col-md-3 jumbotron col-centered" style="margin-top: 12%;">
            <div class="form-group">
                <asp:TextBox ID="txtUserName" runat="server" class="form-control" placeholder="User Name" Text="admin"/>
            </div>
            <div class="form-group">
                <asp:TextBox ID="txtPassword" class="form-control" runat="server" oncopy="return false" onpaste="return false"
                    oncut="return false" placeholder="Password" Text="myfashions" TextMode="Password"/>
            </div>
            <div class="form-group">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit"  class="btn btn-success btn-md btn-block" OnClick="btnSubmit_Click"/>
                <asp:Button ID="btnCancel" runat="server" Text="Clear" class="btn btn-default btn-md btn-block" OnClick="btnCancel_Click"/>
            </div>
            <div class="form-group">
                <asp:LinkButton ID="lnkForgetPwd" runat="server" Text="Forget Password" Visible="false" OnClick="lnkForgetPwd_Click"/>
            </div>
        </div>
    </form>
</body>
</html>
