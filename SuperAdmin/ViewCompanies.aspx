﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/AdminMasterPage.master" AutoEventWireup="true" CodeFile="ViewCompanies.aspx.cs" Inherits="SuperAdmin_ViewCompanies" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <ol class="breadcrumb">
        <li><a href="AdminHome.aspx">Home</a></li>
        <li>Companies</li>
        <li class="active">View Companies</li>
    </ol>

    <div class="row form-group">
        <div class="table-responsive">
            <asp:GridView ID="gvStoreAdmin" runat="server" DataKeyNames="ID" CssClass="table table-striped table-bordered"
                AutoGenerateColumns="false" AllowPaging="true" PageSize="10"
                OnPageIndexChanging="gvStoreAdmin_PageIndexChanging"
                OnRowDeleting="gvStoreAdmin_RowDeleting"
                OnRowEditing="gvStoreAdmin_RowEditing"
                OnRowCommand="gvStoreAdmin_RowCommand">
                <Columns>
                    <asp:TemplateField HeaderText="USERNAME">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("USERNAME") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NAME">
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%#Eval("NAME") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ISACTIVE">
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%#Eval("ISACTIVE") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NUMEMPLOYEES">
                        <ItemTemplate>
                            <asp:Label ID="Label5" runat="server" Text='<%#Eval("NUMEMPLOYEES") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="LASTUPDATEDTIME">
                        <ItemTemplate>
                            <asp:Label ID="Label7" runat="server" Text='<%#Eval("LASTUPDATEDTIME") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <div class="col-wd-70">
                                <div class="ed-del-col">
                                    <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Icons/edit.png" CommandName="EDIT"
                                         CommandArgument='<%#Eval("ID") %>' Style="height: 18px;"  data-toggle="tooltip" data-placement="bottom" title="EDIT" />
                                </div>
                                <div class="ed-del-col">
                                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/Icons/delete.png" CommandName="DELETE" CommandArgument='<%#Eval("ID") %>' 
                                        OnClientClick="return ConfirmDelete();"  Style="height: 18px;" data-toggle="tooltip" data-placement="bottom" title="DELETE" />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="Pagination" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" />
                <EmptyDataTemplate>
                    <ul class="list-group" style="text-align: center;">
                        <li class="list-group-item">No Records Found</li>
                    </ul>
                </EmptyDataTemplate>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

