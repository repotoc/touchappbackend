﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AddCompany.aspx.cs" Inherits="SuperAdmin_AddCompany" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script type="text/javascript">
         $(function () {
             $("#chkShowPassword").bind("click", function () {
                 var txtPassword = $("[id*=txtPassword]");
                 if ($(this).is(":checked")) {
                     alertify.message(txtPassword.val());
                     txtPassword.after('<input class="form-control" onchange = "PasswordChanged(this);" id = "txt_' + txtPassword.attr("id") + '" type = "text" value = "' + txtPassword.val() + '" />');
                     txtPassword.hide();
                 } else {
                     alertify.message(txtPassword.val());
                     txtPassword.val(txtPassword.next().val());
                     txtPassword.next().remove();
                     txtPassword.show();
                 }
             });
         });
         function PasswordChanged(txt) {
             $(txt).prev().val($(txt).val());
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <ol class="breadcrumb">
        <li><a href="SuperAdminHome.aspx">Home</a></li>
        <li>Companies</li>
        <li class="active">Add New Company</li>
    </ol>

    <div class="row form-group">
        <div class="form-group col-md-12 col-sm-12">
             <div class="form-group col-md-6 col-sm-12">
                <div class="col-md-4 col-sm-12"><div class="lblSideHeading">Company ID</div></div>
                <div class="col-md-8 col-sm-12">
                    <asp:TextBox ID="txtCompanyId" runat="server" CssClass="form-control" placeholder="Company ID"></asp:TextBox>
                </div>
            </div>
            <div class="form-group col-md-6 col-sm-12">
                <div class="col-md-4 col-sm-12"><div class="lblSideHeading">Company Name</div></div>
                <div class="col-md-8 col-sm-12">
                    <asp:TextBox ID="txtcompanyName" runat="server" CssClass="form-control" placeholder="Company Name"></asp:TextBox>
                </div>
            </div>
            <div class="form-group col-md-6 col-sm-12">
                <div class="col-md-4 col-sm-12"><div class="lblSideHeading">User Name</div></div>
                <div class="col-md-8 col-sm-12">
                    <asp:TextBox ID="txtusername" runat="server" CssClass="form-control" placeholder="User Name"></asp:TextBox>
                </div>
            </div>

            <div class="form-group col-md-6 col-sm-12">
                <div class="col-md-4 col-sm-12"><div class="lblSideHeading">Password</div></div>
                <div class="col-md-8 col-sm-12">
                    <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                </div>
            </div>
            <div class="form-group col-md-6 col-sm-12">
                <div class="col-md-4 col-sm-12"><div class="lblSideHeading">City</div></div>
                <div class="col-md-8 col-sm-12">
                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" placeholder="City"></asp:TextBox>
                </div>
            </div>
            <div class="form-group col-md-6 col-sm-12">
                <div class="col-md-4 col-sm-12"><div class="lblSideHeading">Address Line1</div></div>
                <div class="col-md-8 col-sm-12">
                    <asp:TextBox ID="txtadd1" runat="server" CssClass="form-control" placeholder="Address Line1" />
                </div>
            </div>

            <div class="form-group col-md-6 col-sm-12">
                <div class="col-md-4 col-sm-12"><div class="lblSideHeading">State</div></div>
                <div class="col-md-8 col-sm-12">
                    <asp:TextBox ID="txtState" runat="server" CssClass="form-control" placeholder="State"></asp:TextBox>
                </div>
            </div>
            <div class="form-group col-md-6 col-sm-12">
                <div class="col-md-4 col-sm-12"><div class="lblSideHeading">Address Line2</div></div>
                <div class="col-md-8 col-sm-12">
                    <asp:TextBox ID="txtadd2" runat="server" class="form-control" placeholder="Address Line2" />
                </div>
            </div>
            <div class="form-group col-md-6 col-sm-12">
                <div class="col-md-4 col-sm-12"><div class="lblSideHeading">No. of Employees</div></div>
                <div class="col-md-8 col-sm-12">
                    <asp:TextBox ID="txtnoemp" runat="server" class="form-control" />
                </div>
            </div>

            <div class="form-group col-md-6 col-sm-12">
                <div class="col-md-4 col-sm-12"><div class="lblSideHeading">Logo</div></div>
                <div class="col-md-8 col-sm-12">
                    <asp:FileUpload ID="fplogo" runat="server" CssClass="form-control" 
                    data-toggle="tooltip" data-placement="bottom" title="select only '.png' format" />
                    <asp:HiddenField ID="hdGuid" runat="server" />
                </div>
            </div>
            <div class="form-group col-md-12 col-sm-12">
                <div class="col-md-2 col-sm-12"><div class="lblSideHeading"></div></div>
                <div class="col-md-8 col-sm-12">
                    <asp:CheckBox ID="chkActive" runat="server" Checked="true" Text="IsActive" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group col-md-12 col-sm-12">
        <div class="col-md-2 col-sm-12"></div>
        <div class="col-md-6 col-sm-6">
                <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-success" OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-default" OnClick="btnCancel_Click" />
        </div>
    </div>
</asp:Content>

