﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyfashionsDB.Models;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Data;

public partial class SuperAdmin_ViewCompanies : System.Web.UI.Page
{
    object lockTarget = new object();
    DataTable dtStoreAdmin;
    string frameURI;
    HttpClient client;
    public static List<Company> companyLst;
    string errorMsg;
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;
        if (!IsPostBack)
        {
            LoadData();
        }
    }

    private void LoadData()
    {        
        #region GetCompanyList
        //Get AdminId from session
        long adminID = Convert.ToInt64(Session["superadminId"]);
        //Get Company List based on AdminID
        string uri = "api/CompanyLogin/GetAllCompanies?superadminid=" + adminID + "";
        HttpResponseMessage response = client.GetAsync(uri).Result;
        #endregion
        if (response.StatusCode.Equals(HttpStatusCode.OK))
        {
            companyLst = response.Content.ReadAsAsync<List<Company>>().Result;
            //Create a datatable to bind all companies
            dtStoreAdmin = new DataTable();
            //create datatable columns list
            var Columns = new[] {
            new DataColumn("ID", typeof(long)),
            new DataColumn("USERNAME", typeof(string)),
            new DataColumn("NAME", typeof(string)),
            new DataColumn("ISACTIVE", typeof(bool)),
            new DataColumn("NUMEMPLOYEES", typeof(int)),
            new DataColumn("VERSION", typeof(int)),
            new DataColumn("LASTUPDATEDTIME", typeof(DateTime)),
        };
            dtStoreAdmin.Columns.AddRange(Columns);         //Add the columns list to datatable
            lock (lockTarget)
            {
                foreach (var item in companyLst.ToList().OrderByDescending(c => c.LASTUPDATEDTIME))
                {
                    DataRow dtRow = dtStoreAdmin.NewRow();
                    dtRow["ID"] = item.ID;
                    dtRow["USERNAME"] = item.USERNAME;
                    dtRow["NAME"] = item.NAME;
                    dtRow["ISACTIVE"] = item.ISACTIVE;
                    dtRow["NUMEMPLOYEES"] = item.NUMEMPLOYEES;
                    dtRow["VERSION"] = item.VERSION;
                    dtRow["LASTUPDATEDTIME"] = item.LASTUPDATEDTIME;
                    dtStoreAdmin.Rows.Add(dtRow);
                }
                gvStoreAdmin.DataSource = dtStoreAdmin;
                gvStoreAdmin.DataBind();
            }
        }
        else
        {
            errorMsg = response.Content.ReadAsStringAsync().Result;
            ClientScript.RegisterStartupScript(GetType(), "notifier10", "errorAlert('" + errorMsg + "');", true);
        }
    }
    protected void gvStoreAdmin_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        LoadData();
        gvStoreAdmin.PageIndex = e.NewPageIndex;
    }
    protected void gvStoreAdmin_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //get the selected company ID from CommandArgument
        long ID = Convert.ToInt64(e.CommandArgument);
        Company company = companyLst.ToList().Where(c => c.ID.Equals(ID)).FirstOrDefault();
        if (e.CommandName == "EDIT")
        {
            if (company != null)
                Response.Redirect("AddCompany.aspx?ID=" + ID + "");
        }
        else if (e.CommandName == "DELETE")
        {
            if (company != null)
            {
                if (company.LSTSTORES != null && company.LSTSTORES.Count > 0)
                {
                    ClientScript.RegisterStartupScript(GetType(), "warning", "warningAlert('Store Link Exists.');", true);
                }
                else
                {

                    #region CRUDOperationCreateCompany
                    string uri = "api/CompanyLogin/CompanyCRUDOperation?parameter=D";
                    HttpResponseMessage response = client.PostAsJsonAsync<Company>(uri, company).Result;
                    if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Company Deleted Successfully');", true);
                    }
                    else
                    {
                        //string str= Newtonsoft.Json.JsonConvert.SerializeObject(company);
                        errorMsg = response.Content.ReadAsStringAsync().Result;
                        ClientScript.RegisterStartupScript(GetType(), "notifier12", "errorAlert('" + errorMsg + "');", true);
                    }
                    #endregion
                    LoadData();
                }
            }
        }
    }
    protected void gvStoreAdmin_RowEditing(object sender, GridViewEditEventArgs e)
    {

    }
    protected void gvStoreAdmin_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
}