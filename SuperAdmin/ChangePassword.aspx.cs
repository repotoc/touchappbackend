﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SuperAdmin_ChangePassword : System.Web.UI.Page
{
    string frameURI;
    HttpClient client; 
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string username = Session["superadminusername"].ToString();
            App.Logger.Info("Start of btnSubmit_Click in _Default page");
            string oldpwd = txtoldpwd.Text;
            string newpassword = txtnewpwd.Text;
            string cnfpwd = txtcnfpwd.Text;
            if (newpassword == cnfpwd)
            {
                string uri = "api/SuperAdmin/ChangeSuperAdminPasword?username=" + username + "&oldpassword=" + oldpwd + "&newpassword=" + newpassword + "";
                HttpResponseMessage response = client.GetAsync(uri).Result;
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    divSuccessRedirectionMsg.Visible = true;
                    ClearAll();
                    ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Changed Password Successfully');setInterval(function(){window.location='../Default.aspx';},2000);", true);
                }
                else
                {
                    string errorMsg = response.Content.ReadAsStringAsync().Result;
                    ClientScript.RegisterStartupScript(GetType(), "notifier1", "errorAlert('Enter valid Username / Password');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "notifier1", "errorAlert('New Password and Confirm Password should Match');", true);
            }
           
            App.Logger.Info("End of btnSubmit_Click in _Default page");
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(GetType(), "error", "errorAlert('" + ex.Message + "');", true);
        }
    }
    private void ClearAll()
    {
        txtcnfpwd.Text = string.Empty;
        txtnewpwd.Text = string.Empty;
        txtoldpwd.Text = string.Empty;
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        ClearAll();
    }
}