﻿
    $(document).ready(function () {
        
        $(".imageupload").change(function () {
          
                var imgup = $(".imageupload").val();
                var pngimg = imgup.split(".");
                if (imgup != "") {
                    if (pngimg[1] != "png") {
                        $(".imgerror").text("Please Choose PNG Format Only");

                        return false;
                    }
                    else {
                        $(".imgerror").text("");
                    }
                }
                else {
                    $(".imgerror").text("");
                }
            
        })
        $(".maincata").keyup(function () {
            $(".maincataerrfroms").text("");
            var value = $(".maincata").val();
            var a = new RegExp("^([a-zA-Z]{0,})$");
            var nothree = new RegExp("^([a-zA-Z]{0,2})$");
            var nothrity = new RegExp("^([a-zA-Z]{30,})$");
            if (value == "") {
                $(".mainerrorfiled").text("Please Enter Text.");
                return false;
            }
            else {
                if (!a.test(value)) {
                    $(".mainerrorfiled").text("Please Enter Alphabets Only.");
              
                    return false;
                }
                else if (nothree.test(value)) {
                    $(".mainerrorfiled").text("Please Enter Minimum 3 Characters.");
              
                    return false;
                }
                else if (nothrity.test(value)) {
                    $(".mainerrorfiled").text("Limit Should Not Exceed 30 Characters.");
                    localStorage.setItem("errormasg", "Limit Should Not Exceed 30 Characters.");
                    return false;
                }
                else {
                    $(".mainerrorfiled").text("");
                }
            }

        });
        $(".savebtn").click(function () {
            $(".maincataerrfroms").text(" ");
            var count = 0;
            var imgup = $(".imageupload").val();
            var value = $(".maincata").val();
            var a = new RegExp("^([a-zA-Z]{0,})$");
            var nothree = new RegExp("^([a-zA-Z]{0,2})$");
            var nothrity = new RegExp("^([a-zA-Z]{30,})$");
            if ($(".savebtn").attr("value") == "Submit") {
                if (imgup == "") {
                    $(".imgerror").text("Please Choose Image")
                    count = count + 1;

                }
            }

            if (value == "") {


                $(".mainerrorfiled").text("Please Enter Text.");
                count = count + 1;
            }
            else {
                if (!a.test(value)) {
                    $(".mainerrorfiled").text("Please Enter Alphabets Only.");
                    count = count + 1;
                }
                else if (nothree.test(value)) {
                    $(".mainerrorfiled").text("Please Enter Minimum 3 Characters.");
                    count = count + 1;
                }
                else if (nothrity.test(value)) {
                    $(".mainerrorfiled").text("Limit Should Not Exceed 30 Characters.");
                    count = count + 1;
                }
                else {
                    $(".mainerrorfiled").text("");
                }
            }
            if (count > 0) {
       
                return false;
            }
        })
   

    })

    