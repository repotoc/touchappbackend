﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyfashionsDB.Models;
using System.Net.Http;
using System.Net.Http.Headers;

public partial class storeLoginPage : System.Web.UI.Page
{
    string frameURI;
    HttpClient client; 
    protected void Page_Load(object sender, EventArgs e)
    {
        frameURI = "http://183.82.0.20:2234/MyFashionsProdAPI/";
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        Uri baseAddress = new Uri(frameURI);
        client.BaseAddress = baseAddress;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            App.Logger.Info("Start of btnSubmit_Click in _Default page");
            string username = txtUserName.Text;
            string password = txtPassword.Text;
            string uri = "api/StoreLogin/ValidateStoreLogin?username=" + username + "&password=" + password + "";
            HttpResponseMessage response = client.GetAsync(uri).Result;
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                string ID = response.Content.ReadAsStringAsync().Result;
                Session["storeadminid"] = ID;
                Session["storeadminname"] = txtUserName.Text;
                Response.Redirect("UserHome.aspx");
            }
            else
            {
                string errorMsg = response.Content.ReadAsStringAsync().Result;
                ClientScript.RegisterStartupScript(GetType(), "notifier1", "errorAlert('Enter valid Username / Password');", true);
            }
            App.Logger.Info("End of btnSubmit_Click in _Default page");
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(GetType(), "error", "errorAlert('" + ex.Message + "');", true);
        }
    }

    private void clearAll()
    {
        txtUserName.Text = string.Empty;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        App.Logger.Info("Start of btnCancel_Click in _Default page");
        txtUserName.Text = txtPassword.Text = string.Empty;
        ClientScript.RegisterStartupScript(GetType(), "notifier2", "messageAlert('Data has been cleared');", true);
        App.Logger.Info("End of btnCancel_Click in _Default page");
    }
    protected void lnkForgetPwd_Click(object sender, EventArgs e)
    {
        App.Logger.Info("Start of lnkForgetPwd_Click in _Default page");
        Response.Redirect("ForgetPassword.aspx");
        App.Logger.Info("End of lnkForgetPwd_Click in _Default page");
    }
}